if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TranAdjustmentExclusions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TranAdjustmentExclusions]
GO

CREATE TABLE [dbo].[TranAdjustmentExclusions] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[MancoProductName] [varchar](50) NOT NULL ,
	[MancoFundName] [varchar](50) NOT NULL ,
	[Active] bit NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TranAdjustmentExclusions] WITH NOCHECK ADD 
	CONSTRAINT [TranAdjustmentExclusions_PK] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
GO


insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed LS Variable','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed LS Property CPI','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Income Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Standard Money Market','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Global Managed','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed FR US Equity','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed FR UK Equity','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed FR Continental Europe','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Equity Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Small Cap Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib ALSI 40 Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Bond Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Value Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Income Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed CPI Plus','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Low Eq FOF','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Property Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Income Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Property Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Financials Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Flexible Income','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed FR US Small Cap','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Property Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Prosperity Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Stability Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Income Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Equity Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Managed Flexible Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Value Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Quants Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Bond Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Income Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM High Equity FOF','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Med Eq FOF','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Low Eq FOF','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Property Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Flexible Income','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Income Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Prosperity Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Stability Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Income Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Standard Money Market','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Bond Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib Value Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Low Eq FOF','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Stanlib MM Property Fund','MILLENIUM, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('RA LS Property CPI','MILLENIUM, PRESERVATION',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('RA Ermitage Alpha Fund','MILLENIUM, PRESERVATION',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('Taxed Standard Money Market','MATURELINK, INDIVIDUAL',1)
insert into TranAdjustmentExclusions (MancoFundName, MancoProductName,  Active) values ('RA Standard Money Market','MATURELINK, RETIREMENT',1)
GO

