# README #

1. If the TrainingDB exists, run script "RemoveDatabaseIfExists.sql", to kill all connections to the DB and delete the DB.
   This is to ensure that all tables are started with no extra information in them from the past.

2. Run the database create script "Update 2006-10-17 17h30 AD - Create Database.sql", make sure that the Filelocation does exist in the script.  
   If not change the script so that the Filelocation does match your Microsoft SQL Server installation paths.

3. Run all the other scripts in "C:\Clients\Digiata Implementation\Training2\LINX Training\3-TrainingDB Setup" with the DBUpdated tool to populate the tables required for the exercises to follow.

4. Refresh the TrainingDB and run a SELECT query against the following table: [TrainingDB].[dbo].[UpdateScriptLog]. 
   The results returned should show a list of all the scripts executed against the DB.

5. Make sure all the objects did populate correctly. 