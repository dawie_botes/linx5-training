if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_WeatherInfo_Temperature_WeatherInfo_CityLookup]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[WeatherInfo_Temperature] DROP CONSTRAINT FK_WeatherInfo_Temperature_WeatherInfo_CityLookup
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WeatherInfo_CityLookup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[WeatherInfo_CityLookup]
GO

CREATE TABLE [dbo].[WeatherInfo_CityLookup] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[CityCode] [varchar] (10) NOT NULL ,
	[CityName] [varchar] (100) NOT NULL ,
	[Country] [varchar] (5) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[WeatherInfo_CityLookup] WITH NOCHECK ADD 
	CONSTRAINT [WeatherInfo_CityLookup_PK] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
GO

CREATE  UNIQUE  INDEX [idx_citycode] ON [dbo].[WeatherInfo_CityLookup]([CityCode]) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WeatherInfo_TemperatureException]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[WeatherInfo_TemperatureException]
GO

CREATE TABLE [dbo].[WeatherInfo_TemperatureException] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[Date] [DateTime] NOT NULL ,
	[CityCode] [varchar] (10) NOT NULL ,
	[TemperatureType] [char] (1) NOT NULL ,
	[Temperature] [decimal] (18,2) NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[WeatherInfo_TemperatureException] WITH NOCHECK ADD 
	CONSTRAINT [WeatherInfo_TemperatureException_PK] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
GO

CREATE  UNIQUE  INDEX [idx_date_citycode] ON [dbo].[WeatherInfo_TemperatureException]([Date],[CityCode]) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WeatherInfo_Temperature]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[WeatherInfo_Temperature]
GO

CREATE TABLE [dbo].[WeatherInfo_Temperature] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[Date] [DateTime] NOT NULL ,
	[CityID] [int] NOT NULL ,
	[TemperatureType] [char] (1) NOT NULL ,
	[Temperature] [decimal] (18,2) NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[WeatherInfo_Temperature] WITH NOCHECK ADD 
	CONSTRAINT [WeatherInfo_Temperature_PK] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
GO

ALTER TABLE dbo.WeatherInfo_Temperature ADD CONSTRAINT
	FK_WeatherInfo_Temperature_WeatherInfo_CityLookup FOREIGN KEY
	(
	CityID
	) REFERENCES dbo.WeatherInfo_CityLookup
	(
	ID
	)
GO

CREATE  UNIQUE  INDEX [idx_date_cityid_temperaturetype] ON [dbo].[WeatherInfo_Temperature]([Date],[CityID],[TemperatureType]) ON [PRIMARY]
GO

set nocount on

insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('JHB', 'Johannesburg', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('PT', 'Pretoria', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('POL', 'Polokwane', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('POT', 'Potchefstroom', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('BF', 'Bloemfontein', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('DUR', 'Durban', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('PE', 'Port Elizabeth', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('GE', 'George', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('CT', 'Cape Town', 'RSA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('UP', 'Upington', 'RSA')

insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('LON', 'London', 'UK')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('PAR', 'Paris', 'FR')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('NY', 'New York', 'USA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('SF', 'San Francisco', 'USA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('SYD', 'Sydney', 'AUS')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('RIO', 'Rio de Janeiro', 'BRA')
insert into WeatherInfo_CityLookup (CityCode, CityName, Country) values ('ZUR', 'Zurich', 'SWI')

set nocount off
