if exists (select * from sysobjects where id = object_id(N'[dbo].[view_WODFileLog]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_WODFileLog]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[view_WOActionPathWOType]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_WOActionPathWOType]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW [dbo].[view_WOActionPathWOType]
AS
SELECT     TOP 100 PERCENT WOType.WOTypeID, WOType.Name AS WOType, WOActionPath.WOActionPathID, WOActionPath.ActionRequired, 
                      WOActionPath.ActionOperator, WOActionPath.ActionSequence, WOAction.WOActionID, WOAction.Name AS WOAction, 
                      WOAction.Description AS WOActionDesc
FROM         WOType INNER JOIN
                      WOActionPath ON WOType.WOTypeID = WOActionPath.WOTypeID INNER JOIN
                      WOAction ON WOActionPath.WOActionID = WOAction.WOActionID
ORDER BY WOType.WOTypeID, WOActionPath.ActionSequence


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE VIEW [dbo].[view_WODFileLog]
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2003/07/18
**PURPOSE		: This view selects data from WODFileLog to be presented in STADIUM as part of the WODetail
**NOTES		:
**
**/

AS
SELECT     
	WODFileLogID, 
	WOID, 
	CFFileSettingID, 
	[FileName] , 
	FileLocation, 
	FileSize, 
	REPLACE (SUBSTRING ( CONVERT (varchar (255), WODFileLog.FileContent), 1,255) , '<xml xmlns:s', '')  AS FileContent, -- changed jkn 2003/11/11 - SUBSTRING (FileContent,1,150) AS FileContent, 
	BackUpFileName, 
	BackupFileLocation
FROM         WODFileLog











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


if exists (select * from sysobjects where id = object_id(N'[dbo].[view_WODFileLog_Order]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_WODFileLog_Order]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[view_WO]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_WO]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[view_WO_Status]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_WO_Status]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE VIEW [dbo].[view_WO]
AS
SELECT     TOP 100 PERCENT WO.WOID, WOType.Name AS WOType, WOStatus.Name AS WOStatus, WOPriority.Name AS WOPriority, 
                      WOAction.Name AS NextAction, WO.NextActionDate, CFInterfaceType.Name AS WOSource
FROM         WOAction INNER JOIN
                      WOActionPath ON WOAction.WOActionID = WOActionPath.WOActionID INNER JOIN
                      WOPriority INNER JOIN
                      WO ON WOPriority.WOPriorityID = WO.WOPriorityID INNER JOIN
                      WOStatus ON WO.WOStatusID = WOStatus.WOStatusID INNER JOIN
                      CFInterfaceType ON WO.WOSource = CFInterfaceType.CFInterfaceTypeID ON WOActionPath.WOTypeID = WO.WOTypeID AND 
                      WOActionPath.ActionSequence = WO.NextActionSequence INNER JOIN
                      WOType ON WO.WOTypeID = WOType.WOTypeID
ORDER BY WO.WOID DESC



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE VIEW [dbo].[view_WO_Status]
AS
SELECT     TOP 100 PERCENT WO.WOID, WOType.Name AS WOType, WOStatus.Name AS Status, WOPriority.Name AS Priority, 
                      WOAction.Name AS NextAction
FROM         WOPriority INNER JOIN
                      WO ON WOPriority.WOPriorityID = WO.WOPriorityID INNER JOIN
                      WOStatus ON WO.WOStatusID = WOStatus.WOStatusID INNER JOIN
                      WOType ON WO.WOTypeID = WOType.WOTypeID INNER JOIN
                      WOActionPath INNER JOIN
                      WOAction ON WOActionPath.WOActionID = WOAction.WOActionID ON WOType.WOTypeID = WOActionPath.WOTypeID AND 
                      WO.NextActionSequence = WOActionPath.ActionSequence
ORDER BY WO.WOID DESC




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW [dbo].[view_WODFileLog_Order]
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2003/07/18
**PURPOSE		: This view selects data from WODFileLog to be presented in STADIUM
**NOTES		:
**
**/
AS
SELECT     TOP 100 PERCENT 
	WODFileLog.WODFileLogID, 
	WODFileLog.WOID, 
	WOStatus.Name AS Status, 
	WOHistory.WOActionDate, 
        WOAction.Name AS [Action], 
	WODFileLog.FileName AS FileName, 
	WODFileLog.FileSize, 
	WODFileLog.FileLocation, 
	WODFileLog.BackUpFileName, 
	WODFileLog.BackupFileLocation, 
	WODFileLog.CFFileSettingID

FROM         WODFileLog INNER JOIN
                      WO ON WODFileLog.WOID = WO.WOID INNER JOIN
                      WOHistory ON WO.WOID = WOHistory.WOID INNER JOIN
                      WOAction ON WOHistory.WOActionID = WOAction.WOActionID INNER JOIN
                      WOStatus ON WO.WOStatusID = WOStatus.WOStatusID
WHERE     
	(WOHistory.WOActionID = 1) 
	AND (DATEDIFF(d, CURRENT_TIMESTAMP, WOHistory.WOActionDate) = 0)

ORDER BY WODFileLog.WODFileLogID DESC







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

