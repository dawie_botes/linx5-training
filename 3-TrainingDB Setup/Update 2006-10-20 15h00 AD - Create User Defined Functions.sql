if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_IsBusinessDay]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_IsBusinessDay]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_NextBusinessDay]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_NextBusinessDay]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_PrevBusinessDay]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_PrevBusinessDay]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_is_leap_year]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_is_leap_year]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION [dbo].[fn_IsBusinessDay] 
(
	@T datetime,
	@Country varchar(30)
)  
/*
@T is the current day, i.e. now

Return 1 if the given day is a business day in the given country
and return 0 if it is a weekend or public holiday.
*/

RETURNS BIT AS  

BEGIN 

	declare @IsBusinessDay bit

	if 	datename(weekday, @T) in ('SATURDAY', 'SUNDAY') 
		or exists(select [Date] from CFPublicHoliday where Country = @Country and [Date] = @T)
		or 
		(
		@Country = 'SA'
		and
		datename(weekday, dateadd(day, -1, @T)) = 'SUNDAY'
		and
		exists(select [Date] from CFPublicHoliday where Country = @Country and [Date] = dateadd(day, -1, @T))
		)

		set @IsBusinessDay = 0
	else
		set @IsBusinessDay = 1

	return @IsBusinessDay
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION [dbo].[fn_NextBusinessDay] 
(
	@T datetime,
	@Country varchar(30),
	@N int
)  
/*
This function add N business days to the current date and return it,
i.e. it returns T+N business days

@T is the current date
@N is the number of business days that must be added to @T
*/

RETURNS DATETIME AS  

BEGIN 

	declare @counter int, @NBD datetime

	set @counter = 1
	set @NBD = @T

	while (@counter <= @N)
	begin
		set @NBD = dateadd(day, 1, @NBD)
		if [dbo].[fn_IsBusinessDay](@NBD, @Country) = 1 set @counter = @counter + 1
	end
	
	return @NBD
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION [dbo].[fn_PrevBusinessDay] 
(
	@T datetime,
	@Country varchar(30),
	@N int
)  
/*
This function substract N business days from the current date and return it,
i.e. it returns T-N business days

@T is the current date
@N is the number of business days that must be substracted from @T
*/

RETURNS DATETIME AS  

BEGIN 
	declare @counter int, @PBD datetime

	set @counter = 1
	set @PBD = @T

	while (@counter <= @N)
	begin
		set @PBD = dateadd(day, -1, @PBD)
		if [dbo].[fn_IsBusinessDay](@PBD, @Country) = 1 set @counter = @counter + 1
	end
	
	return @PBD
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE FUNCTION dbo.fn_is_leap_year(@year INTEGER)
RETURNS BIT AS 
BEGIN

  IF @year % 400 = 0
     -- Years divisible by 400 (e.g. 1600, 2000) are always leap years
     RETURN 1 
  ELSE
  BEGIN
    IF @year % 100 = 0
       -- Years not divisible by 400 but divisible by 100 (e.g. 1900) are never leap years
       RETURN 0
    ELSE
    BEGIN
      IF @year % 4 = 0
         -- Years not divisible by 400 or 100 but divisible by 4 (e.g. 1976) are always leap years
         RETURN 1
      ELSE
         RETURN 0
    END
  END

  -- The following statement should never be reached (but the SQL syntax parser requires it)
  RETURN 0 

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_ParseListToTable]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_ParseListToTable]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION [dbo].[fn_ParseListToTable]
	(@DelimitedList varchar(8000), 
	@Delimiter as varchar(5))

RETURNS @fn_ParseListToTable TABLE (ElementOrder int, ElementValue varchar(250))

AS
BEGIN
	declare @ListLength int, @ElementStart int, @ElementEnd int, @ElementOrder int

	--initalise variables	
	set @DelimitedList = ltrim(rtrim(@DelimitedList))
	if right(@DelimitedList,1) <> @Delimiter set @DelimitedList = @DelimitedList + @Delimiter
	set @ListLength = len(@DelimitedList)
	set @ElementStart = 1
	set @ElementOrder = 1
	
	--parse delimited input string
	if @ListLength > 0
	begin
		while   @ElementStart <= @ListLength
		begin
			select @ElementEnd = charindex(@Delimiter, @DelimitedList, @ElementStart) - 1
			
			insert into @fn_ParseListToTable (ElementOrder, ElementValue) 
			values (@ElementOrder,  ltrim(rtrim(substring(@DelimitedList, @ElementStart, (@ElementEnd - @ElementStart + 1)))))
			
			set @ElementStart = @ElementEnd + 2
			set @ElementOrder = @ElementOrder + 1
		end
	end

	return
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

