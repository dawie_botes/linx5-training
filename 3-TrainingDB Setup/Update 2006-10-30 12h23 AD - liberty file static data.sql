declare @CFFormatTypeID int,
	@CFFormatID int,
	@CFSystemID int,
	@CFTradingPartnerID int,
	@CFFileSettingID int

select	@CFFormatTypeID = 1,
	@CFFormatID = 1,
	@CFSystemID = 1,
	@CFTradingPartnerID = 1,
	@CFFileSettingID = 1

delete from CFFileSetting where CFFileSettingID = @CFFileSettingID
delete from CFTradingPartner where CFTradingPartnerID = @CFTradingPartnerID
delete from CFSystem where CFSystemID = @CFSystemID
delete from CFFormat where CFFormatID = @CFFormatID
delete from CFFormatType where CFFormatTypeID = @CFFormatTypeID

set identity_insert CFFormatType on
insert into CFFormatType (CFFormatTypeID, [Name], [Description]) values (@CFFormatTypeID, 'Liberty Life', 'Liberty Life Daily Multivest Statement File')
set identity_insert CFFormatType off

set identity_insert CFFormat on
insert into CFFormat (CFFormatID, [Name], [Description], CFFormatTypeID) 
values (@CFFormatID, 'Liberty Life', 'Liberty Life Daily Multivest Statement File', @CFFormatTypeID)
set identity_insert CFFormat off

set identity_insert CFSystem on
insert into CFSystem (CFSystemID, [Name], [Description]) 
values (@CFSystemID, 'Training', 'Training')
set identity_insert CFSystem off

set identity_insert CFTradingPartner on
insert into CFTradingPartner (CFTradingPartnerID, [Name], [Description], InternalPartner) 
values (@CFTradingPartnerID, 'Training', 'Training','')
set identity_insert CFTradingPartner off

set identity_insert CFFileSetting on
insert into CFFileSetting 
(CFFileSettingID, CFTradingPartnerID, CFSystemID, CFFormatID, [Name], [Description],
[FileName], [FileLocation], [BackupFileName], [BackupFileLocation], [ConstraintAlwaysCreated],
[ConstraintFreq], [ContraintFromTime], [ContraintToTime], [TemplateLocation])
values 
(@CFFileSettingID, @CFTradingPartnerID, @CFSystemID, @CFFormatID, 'Liberty File', 'Liberty Life Daily Multivest Statement File',
'LibertyFile.xls', 'C:\Clients\Digiata\Training\LINX\SourceFiles','','',
'N','Daily','00:00:00 AM','11:59:00 PM', NULL)
set identity_insert CFFileSetting off

/*
select * from CFTradingPartner
select * from CFSystem
select * from CFFormat
select * from CFFormatType
select * from CFFileSetting
*/