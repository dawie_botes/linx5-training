USE [master]
GO
--===================================================================================
-- Create the sql to kill the active database connections
declare @execSql varchar(1000), @databaseName varchar(100)
-- Set the database name for which to kill the connections
set @databaseName = 'TrainingDB'

set @execSql = '' 
select  @execSql = @execSql + 'kill ' + convert(char(10), spid) + ' '
from    master.dbo.sysprocesses
where   db_name(dbid) = @databaseName
     and
     DBID <> 0
     and
     spid <> @@spid
exec(@execSql)
--===================================================================================
/****** Object:  Database [TrainingDB]    Script Date: 01/06/2015 22:13:43 ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'TrainingDB')
DROP DATABASE [TrainingDB]
GO