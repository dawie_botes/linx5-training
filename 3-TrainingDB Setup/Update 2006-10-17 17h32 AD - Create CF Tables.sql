/** TABLES, PRIMARY KEYS AND INDEXES **/

CREATE TABLE [dbo].[CFDbSourceParamSetting] (
	[CFDbSourceParamSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[CFDbSourceSettingID] [int] NOT NULL ,
	[ParamName] [varchar] (50) NOT NULL ,
	[ParamType] [varchar] (50) NOT NULL ,
	[ParamValueDefault] [varchar] (100) NOT NULL ,
	[ParamValuePrevious] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFDbSourceParamSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFDbSourceParamSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFDbSourceParamSettingID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFDbSourceSetting] (
	[CFDbSourceSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[DbConnectionString] [varchar] (200) NOT NULL ,
	[DbObjectType] [varchar] (50) NOT NULL ,
	[DbObjectName] [varchar] (50) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFDbSourceSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFDbSourceSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFDbSourceSettingID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFErrorLog] (
	[CFErrorLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[ErrorMessage] [varchar] (2000) NOT NULL ,
	[ProcessName] [varchar] (100) NOT NULL ,
	[EventName] [varchar] (100) NULL ,
	[ErrorTimeStamp] [datetime] NOT NULL ,
	[WOID] [int] NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFErrorLog] WITH NOCHECK ADD 
	CONSTRAINT [CFErrorLog_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFErrorLogID]
	)  ON [PRIMARY] 
GO
ALTER TABLE [dbo].[CFErrorLog] ADD 
	CONSTRAINT [DF__CFErrorLog_1] DEFAULT ('getdate()') FOR [ErrorTimeStamp]
GO
CREATE  INDEX [IX_CFErrorLog1] ON [dbo].[CFErrorLog]([WOID]) ON [PRIMARY]
GO
CREATE  INDEX [IX_CFErrorLog2] ON [dbo].[CFErrorLog]([ErrorTimeStamp]) ON [PRIMARY]
GO


CREATE TABLE [dbo].[CFFTPSetting] (
	[CFFTPSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[CFFileSettingID] [int] NOT NULL ,
	[HostName] [varchar] (50) NOT NULL ,
	[User] [varchar] (50) NOT NULL ,
	[Password] [varchar] (50) NOT NULL ,
	[Port] [varchar] (50) NOT NULL ,
	[RemotePath] [varchar] (250) NOT NULL ,
	[FileName] [varchar] (100) NOT NULL ,
	[DestinationPath] [varchar] (250) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFFTPSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFFTPSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFFTPSettingID]
	)  ON [PRIMARY] 
GO


CREATE TABLE [dbo].[CFFileSetting] (
	[CFFileSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFTradingPartnerID] [int] NOT NULL ,
	[CFSystemID] [int] NOT NULL ,
	[CFFormatID] [int] NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[FileName] [varchar] (100) NOT NULL ,
	[FileLocation] [varchar] (200) NOT NULL ,
	[BackupFileName] [varchar] (100) NOT NULL ,
	[BackupFileLocation] [varchar] (200) NOT NULL ,
	[ConstraintAlwaysCreated] [char] (1) NOT NULL ,
	[ConstraintFreq] [varchar] (50) NOT NULL ,
	[ContraintFromTime] [varchar] (50) NOT NULL ,
	[ContraintToTime] [varchar] (50) NOT NULL ,
	[TemplateLocation] [varchar] (200) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFFileSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFFileSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFFileSettingID]
	)  ON [PRIMARY] 
GO


CREATE TABLE [dbo].[CFFormat] (
	[CFFormatID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (200) NOT NULL ,
	[CFFormatTypeID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFFormat] WITH NOCHECK ADD 
	CONSTRAINT [CFFormat_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFFormatID]
	)  ON [PRIMARY] 
GO
	
CREATE TABLE [dbo].[CFFormatDetailFax] (
	[CFFormatDetailFaxID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFFormatID] [int] NOT NULL ,
	[FullContent] [text] NULL ,
	[ProdBlock] [text] NULL ,
	[ProdPrefix] [text] NULL ,
	[ProdSuffix] [varchar] (8000) NULL ,
	[ProdSeparator] [varchar] (8000) NULL ,
	[TranBlock] [varchar] (8000) NULL ,
	[TranPrefix] [varchar] (8000) NULL ,
	[TranSuffix] [varchar] (8000) NULL ,
	[TranSeparator] [varchar] (8000) NULL ,
	[ProdFiller] [text] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFFormatDetailFax] WITH NOCHECK ADD 
	CONSTRAINT [CFFormatDetailFax_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFFormatDetailFaxID]
	)  ON [PRIMARY] 
GO


CREATE TABLE [dbo].[CFFormatType] (
	[CFFormatTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFFormatType] WITH NOCHECK ADD 
	CONSTRAINT [CFFormatType_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFFormatTypeID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFGeneralSetting] (
	[CFGeneralSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[SettingName] [varchar] (50) NOT NULL ,
	[SettingValue] [varchar] (50) NOT NULL ,
	[SettingDescription] [varchar] (150) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFGeneralSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFGeneralSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFGeneralSettingID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_CFGeneralSetting1] ON [dbo].[CFGeneralSetting]([SettingName]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CFHTTPParamSetting] (
	[CFHTTPParamSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[CFHTTPSettingID] [int] NOT NULL ,
	[ParamName] [varchar] (50) NOT NULL ,
	[ParamValueDefault] [varchar] (500) NOT NULL ,
	[ParamValuePrevious] [varchar] (500) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFHTTPParamSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFHTTPParamSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFHTTPParamSettingID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFHTTPSetting] (
	[CFHTTPSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[CFSystemID] [int] NOT NULL ,
	[URLString] [varchar] (500) NOT NULL ,
	[URLActive] [bit] NOT NULL ,
	[SortSeq] [int] NOT NULL ,
	[PreviousRunDate] [datetime] NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFHTTPSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFHTTPSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFHTTPSettingID]
	)  ON [PRIMARY] 
GO


CREATE TABLE [dbo].[CFInterfaceType] (
	[CFInterfaceTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFInterfaceType] WITH NOCHECK ADD 
	CONSTRAINT [CFInterfaceType_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFInterfaceTypeID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFLinxProcess] (
	[CFLinxProcessID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (250) NOT NULL ,
	[Protocol] [varchar] (50) NOT NULL ,
	[Port] [varchar] (50) NOT NULL ,
	[LINXServer] [varchar] (50) NOT NULL ,
	[ServerUser] [varchar] (50) NOT NULL ,
	[ServerPassword] [varchar] (50) NOT NULL ,
	[SolutionFile] [varchar] (250) NOT NULL ,
	[SolutionName] [varchar] (100) NOT NULL ,
	[Project] [varchar] (100) NOT NULL ,
	[Process] [varchar] (100) NOT NULL ,
	[ProcessActive] [bit] NOT NULL ,
	[ProcessInvoke] [bit] NOT NULL ,
	[SortSeq] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFLinxProcess] WITH NOCHECK ADD 
	CONSTRAINT [CFLinxProcesses_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFLinxProcessID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFLinxProcessLog] (
	[CFLinxProcessLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFLinxProcessID] [int] NOT NULL ,
	[CurrentDate] [datetime] NULL ,
	[CFLinxProcessStatusID] [int] NULL ,
	[Notes] [varchar] (100) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFLinxProcessLog] WITH NOCHECK ADD 
	CONSTRAINT [CFLinxProcessLog_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFLinxProcessLogID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_CFLinxProcessLog1] ON [dbo].[CFLinxProcessLog]([CFLinxProcessID]) ON [PRIMARY]
GO
CREATE  INDEX [IX_CFLinxProcessLog2] ON [dbo].[CFLinxProcessLog]([CurrentDate]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CFLinxProcessLogParameter] (
	[CFLinxProcessLogParameterID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFLinxProcessID] [int] NOT NULL ,
	[CFLinxProcessLogID] [int] NOT NULL ,
	[ParameterValue] [varchar] (50) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFLinxProcessLogParameter] WITH NOCHECK ADD 
	CONSTRAINT [CFLinxProcessLogParameter_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFLinxProcessLogParameterID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_CFLinxProcessLogParameter1] ON [dbo].[CFLinxProcessLogParameter]([CFLinxProcessID],[CFLinxProcessLogID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CFLinxProcessLogStatus] (
	[CFLinxProcessLogStatusID] [int] IDENTITY (1, 1) NOT NULL ,
	[Status] [varchar] (50) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFLinxProcessLogStatus] WITH NOCHECK ADD 
	CONSTRAINT [CFLinxProcessLogStatus_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFLinxProcessLogStatusID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFMailAttachmentClassify] (
	[CFMailAttachmentClassifyID] [int] IDENTITY (1, 1) NOT NULL ,
	[MSender] [varchar] (250) NULL ,
	[MReceiver] [varchar] (250) NULL ,
	[MSubject] [varchar] (1000) NULL ,
	[MBody] [varchar] (1000) NULL ,
	[FileName] [varchar] (250) NULL ,
	[CFFileSettingID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFMailAttachmentClassify] WITH NOCHECK ADD 
	CONSTRAINT [CFMailAttachmentClassify_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFMailAttachmentClassifyID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_CFMailAttachmentClassify1] ON [dbo].[CFMailAttachmentClassify]([CFFileSettingID]) ON [PRIMARY]
GO


CREATE TABLE [dbo].[CFMailAttachmentSetting] (
	[CFMailAttachmentSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFMailSettingID] [int] NOT NULL ,
	[CFFileLocationID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFMailAttachmentSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFMailAttachmentSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFMailAttachmentSettingID]
	)  ON [PRIMARY] 
GO


CREATE TABLE [dbo].[CFMailSetting] (
	[CFMailSettingID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFTradingPartnerID] [int] NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[MDirection] [varchar] (10) NOT NULL ,
	[MSender] [varchar] (100) NOT NULL ,
	[MReceiver] [varchar] (100) NOT NULL ,
	[MCC] [varchar] (100) NULL ,
	[MBCC] [varchar] (100) NULL ,
	[MSubject] [varchar] (1000) NOT NULL ,
	[MBody] [text] NULL ,
	[MAttachmentProcessing] [char] (1) NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFMailSetting] WITH NOCHECK ADD 
	CONSTRAINT [CFMailSetting_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFMailSettingID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFNextRunTime] (
	[CFNextRunTimeID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFLinxProcessID] [int] NULL ,
	[Interval] [int] NULL ,
	[StartTime] [varchar] (8) NULL ,
	[EndTime] [varchar] (8) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFNextRunTime] WITH NOCHECK ADD 
	CONSTRAINT [CFNextRunTime_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFNextRunTimeID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_CFNextRunTime1] ON [dbo].[CFNextRunTime]([CFLinxProcessID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CFPublicHoliday] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[Country] [varchar] (30) NOT NULL ,
	[Date] [datetime] NOT NULL ,
	[Description] [varchar] (50) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFPublicHoliday] WITH NOCHECK ADD 
	CONSTRAINT [CFPublicHoliday_PK] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
GO
CREATE  UNIQUE  INDEX [IX_CFPublicHoliday1] ON [dbo].[CFPublicHoliday]([Country], [Date]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CFSystem] (
	[CFSystemID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFSystem] WITH NOCHECK ADD 
	CONSTRAINT [CFSystem_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFSystemID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[CFTradingPartner] (
	[CFTradingPartnerID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[InternalPartner] [char] (1) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFTradingPartner] WITH NOCHECK ADD 
	CONSTRAINT [CFTradingPartner_PK] PRIMARY KEY  CLUSTERED 
	(
		[CFTradingPartnerID]
	)  ON [PRIMARY] 
GO

/** FOREIGN KEYS **/
ALTER TABLE [dbo].[CFDbSourceParamSetting] ADD 
	CONSTRAINT [CFDbSourceSetting_CFDbSourceParamSetting_FK1] FOREIGN KEY 
	(
		[CFDbSourceSettingID]
	) REFERENCES [dbo].[CFDbSourceSetting] (
		[CFDbSourceSettingID]
	)
GO

ALTER TABLE [dbo].[CFFTPSetting] ADD 
	CONSTRAINT [CFFileSetting_CFFTPSetting_FK1] FOREIGN KEY 
	(
		[CFFileSettingID]
	) REFERENCES [dbo].[CFFileSetting] (
		[CFFileSettingID]
	)
GO

ALTER TABLE [dbo].[CFFileSetting] ADD 
	CONSTRAINT [CFSystem_CFFileSetting_FK1] FOREIGN KEY 
	(
		[CFSystemID]
	) REFERENCES [dbo].[CFSystem] (
		[CFSystemID]
	),
	CONSTRAINT [CFFormat_CFFileSetting_FK1] FOREIGN KEY 
	(
		[CFFormatID]
	) REFERENCES [dbo].[CFFormat] (
		[CFFormatID]
	),
	CONSTRAINT [CFTradingPartner_CFFileSetting_FK1] FOREIGN KEY 
	(
		[CFTradingPartnerID]
	) REFERENCES [dbo].[CFTradingPartner] (
		[CFTradingPartnerID]
	)
GO

ALTER TABLE [dbo].[CFFormat] ADD 
	CONSTRAINT [CFFormatType_CFFormat_FK1] FOREIGN KEY 
	(
		[CFFormatTypeID]
	) REFERENCES [dbo].[CFFormatType] (
		[CFFormatTypeID]
	)
GO

ALTER TABLE [dbo].[CFFormatDetailFax] ADD 
	CONSTRAINT [CFFormat_CFFormatDetailFax_FK1] FOREIGN KEY 
	(
		[CFFormatID]
	) REFERENCES [dbo].[CFFormat] (
		[CFFormatID]
	)
GO

ALTER TABLE [dbo].[CFHTTPParamSetting] ADD 
	CONSTRAINT [CFHTTPSetting_CFHTTPParamSetting_FK1] FOREIGN KEY 
	(
		[CFHTTPSettingID]
	) REFERENCES [dbo].[CFHTTPSetting] (
		[CFHTTPSettingID]
	)
GO

ALTER TABLE [dbo].[CFHTTPSetting] ADD 
	CONSTRAINT [CFSystem_CFHTTPSetting_FK1] FOREIGN KEY 
	(
		[CFSystemID]
	) REFERENCES [dbo].[CFSystem] (
		[CFSystemID]
	)
GO

ALTER TABLE [dbo].[CFMailAttachmentClassify] ADD 
	CONSTRAINT [CFFileSetting_CFMailAttachmentClassify_FK1] FOREIGN KEY 
	(
		[CFFileSettingID]
	) REFERENCES [dbo].[CFFileSetting] (
		[CFFileSettingID]
	)
GO

ALTER TABLE [dbo].[CFMailAttachmentSetting] ADD 
	CONSTRAINT [CFFileSetting_CFMailAttachmentSetting_FK1] FOREIGN KEY 
	(
		[CFFileLocationID]
	) REFERENCES [dbo].[CFFileSetting] (
		[CFFileSettingID]
	),
	CONSTRAINT [CFMailSetting_CFMailAttachmentSetting_FK1] FOREIGN KEY 
	(
		[CFMailSettingID]
	) REFERENCES [dbo].[CFMailSetting] (
		[CFMailSettingID]
	)
GO

ALTER TABLE [dbo].[CFMailSetting] ADD 
	CONSTRAINT [CFTradingPartner_CFMailSetting_FK1] FOREIGN KEY 
	(
		[CFTradingPartnerID]
	) REFERENCES [dbo].[CFTradingPartner] (
		[CFTradingPartnerID]
	)
GO

ALTER TABLE [dbo].[CFNextRunTime] ADD 
	CONSTRAINT [CFLinxProcess_CFNextRunTime_FK1] FOREIGN KEY 
	(
		[CFLinxProcessID]
	) REFERENCES [dbo].[CFLinxProcess] (
		[CFLinxProcessID]
	)
GO

/** SCRIPT COMPLETE **/

