Set Identity_Insert WOPriority ON                                                     
Insert into WOPriority (WOPriorityID, Name, Description) Values (1, 'Normal', 'Normal')
Insert into WOPriority (WOPriorityID, Name, Description) Values (2, 'Low', 'Low')     
Insert into WOPriority (WOPriorityID, Name, Description) Values (3, 'High', 'High')    
Insert into WOPriority (WOPriorityID, Name, Description) Values (4, 'Urgent', 'Urgent')
Set Identity_Insert WOPriority OFF

Set Identity_INsert WOStatus ON
Insert into WOStatus (WOStatusID, Name, Description) Values (1, 'New', 'New WO created')
Insert into WOStatus (WOStatusID, Name, Description) Values (2, 'Active', 'WO in progress')
Insert into WOStatus (WOStatusID, Name, Description) Values (3, 'Pended', 'WO is pended')
Insert into WOStatus (WOStatusID, Name, Description) Values (4, 'Cancelled', 'WO has been cancelled')
Insert into WOStatus (WOStatusID, Name, Description) Values (5, 'Duplicate', 'Duplicate WO')
Insert into WOStatus (WOStatusID, Name, Description) Values (6, 'Complete', 'WO is completed')
Insert into WOStatus (WOStatusID, Name, Description) Values (7, 'Rejected', 'WO is rejected')
Set Identity_insert WOStatus OFF                                              
                                               
                                              
