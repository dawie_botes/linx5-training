
GO
/****** Object:  Table [dbo].[CFFileSettingWOType]    Script Date: 01/28/2010 10:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CFFileSettingWOType](
	[CFFileSettingWOTypeID] [int] IDENTITY(1,1) NOT NULL,
	[CFFileSettingID] [int] NOT NULL,
	[WOTypeID] [int] NOT NULL,
	[WODetailTypeID] [int] NULL,
 CONSTRAINT [PK_CFFileSettingWOType] PRIMARY KEY CLUSTERED 
(
	[CFFileSettingWOTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CFFileSettingWOType]  WITH CHECK ADD  CONSTRAINT [FK_CFFileSettingWOType_CFFileSetting] FOREIGN KEY([CFFileSettingID])
REFERENCES [dbo].[CFFileSetting] ([CFFileSettingID])
GO
ALTER TABLE [dbo].[CFFileSettingWOType] CHECK CONSTRAINT [FK_CFFileSettingWOType_CFFileSetting]
GO
ALTER TABLE [dbo].[CFFileSettingWOType]  WITH CHECK ADD  CONSTRAINT [FK_CFFileSettingWOType_WOType] FOREIGN KEY([WOTypeID])
REFERENCES [dbo].[WOType] ([WOTypeID])
GO
ALTER TABLE [dbo].[CFFileSettingWOType] CHECK CONSTRAINT [FK_CFFileSettingWOType_WOType]
GO
ALTER TABLE [dbo].[CFFileSettingWOType]  WITH CHECK ADD  CONSTRAINT [FK_CFFileSettingWOType_WOTypeDetail] FOREIGN KEY([WODetailTypeID])
REFERENCES [dbo].[WOTypeDetail] ([WODetailTypeID])
GO
ALTER TABLE [dbo].[CFFileSettingWOType] CHECK CONSTRAINT [FK_CFFileSettingWOType_WOTypeDetail]