/** TABLES, PRIMARY KEYS AND INDEXES **/

CREATE TABLE [dbo].[WO] (
	[WOID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOTypeID] [int] NOT NULL ,
	[WOStatusID] [int] NOT NULL ,
	[WOPriorityID] [int] NOT NULL ,
	[NextActionSequence] [int] NOT NULL ,
	[NextActionDate] [datetime] NOT NULL ,
	[WOSource] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WO] WITH NOCHECK ADD 
	CONSTRAINT [WO_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WO1] ON [dbo].[WO]([WOTypeID]) ON [PRIMARY]
GO
CREATE  INDEX [IX_WO2] ON [dbo].[WO]([NextActionSequence]) ON [PRIMARY]
GO


CREATE TABLE [dbo].[WOAction] (
	[WOActionID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOAction] WITH NOCHECK ADD 
	CONSTRAINT [WOAction_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOActionID]
	)  ON [PRIMARY] 
GO


CREATE TABLE [dbo].[WOActionPath] (
	[WOTypeID] [int] NOT NULL ,
	[WOActionID] [int] NOT NULL ,
	[WOActionPathID] [int] IDENTITY (1, 1) NOT NULL ,
	[ActionRequired] [char] (1) NOT NULL ,
	[ActionOperator] [varchar] (50) NOT NULL ,
	[ActionSequence] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOActionPath] WITH NOCHECK ADD 
	CONSTRAINT [WOActionPath_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOActionPathID]
	)  ON [PRIMARY] 
GO
CREATE  UNIQUE  INDEX [IX_WOActionPath1] ON [dbo].[WOActionPath]([WOTypeID], [ActionSequence]) ON [PRIMARY]
GO


CREATE TABLE [dbo].[WOActionPathOptions] (
	[WOActionPathOptionsID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOTypeID] [int] NOT NULL ,
	[ActionSequence] [int] NOT NULL ,
	[PossibleNextActionSequence] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOActionPathOptions] WITH NOCHECK ADD 
	CONSTRAINT [WOActionPathOptions_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOActionPathOptionsID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WOActionPathOptions1] ON [dbo].[WOActionPathOptions]([WOTypeID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WODFaxAttachmentLog] (
	[WODFaxAttachmentLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOID] [int] NOT NULL ,
	[WODFaxLogID] [int] NOT NULL ,
	[WODFileLogID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WODFaxAttachmentLog] WITH NOCHECK ADD 
	CONSTRAINT [WODFaxAttachmentLog_PK] PRIMARY KEY  CLUSTERED 
	(
		[WODFaxAttachmentLogID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WODFaxAttachmentLog1] ON [dbo].[WODFaxAttachmentLog]([WOID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WODFaxLog] (
	[WODFaxLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOID] [int] NOT NULL ,
	[FContactName] [varchar] (250) NOT NULL ,
	[FContactFax] [varchar] (100) NOT NULL ,
	[FBody] [varchar] (250) NULL ,
	[FSubject] [varchar] (100) NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WODFaxLog] WITH NOCHECK ADD 
	CONSTRAINT [WODFaxLog_PK] PRIMARY KEY  CLUSTERED 
	(
		[WODFaxLogID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WODFaxLog1] ON [dbo].[WODFaxLog]([WOID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WODFileLog] (
	[WODFileLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOID] [int] NOT NULL ,
	[CFFileSettingID] [int] NOT NULL ,
	[FileName] [varchar] (200) NOT NULL ,
	[FileLocation] [varchar] (200) NOT NULL ,
	[FileSize] [decimal](10, 2) NULL ,
	[FileContent] [text] NOT NULL ,
	[BackUpFileName] [varchar] (200) NULL ,
	[BackupFileLocation] [varchar] (200) NULL ,
	[FileContentKey] [varchar] (200) NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[WODFileLog] WITH NOCHECK ADD 
	CONSTRAINT [WODFileLog_PK] PRIMARY KEY  CLUSTERED 
	(
		[WODFileLogID] DESC 
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WODFileLog1] ON [dbo].[WODFileLog]([WOID]) ON [PRIMARY]
GO


CREATE TABLE [dbo].[WODMailAttachmentLog] (
	[WODMailAttachmentLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOID] [int] NOT NULL ,
	[WODMailLogID] [int] NOT NULL ,
	[WODFileLogID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WODMailAttachmentLog] WITH NOCHECK ADD 
	CONSTRAINT [WODMailAttachmentLog_PK] PRIMARY KEY  CLUSTERED 
	(
		[WODMailAttachmentLogID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WODMailAttachmentLog1] ON [dbo].[WODMailAttachmentLog]([WOID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WODMailLog] (
	[WODMailLogID] [int] IDENTITY (1, 1) NOT NULL ,
	[CFMailSettingID] [int] NOT NULL ,
	[WOID] [int] NOT NULL ,
	[MDirection] [varchar] (10) NULL ,
	[MSender] [varchar] (100) NULL ,
	[MReceiver] [varchar] (100) NULL ,
	[MCC] [varchar] (100) NULL ,
	[MBCC] [varchar] (100) NULL ,
	[MSubject] [varchar] (3000) NULL ,
	[MBody] [text] NOT NULL ,
	[MAttachmentProcessed] [char] (1) NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[WODMailLog] WITH NOCHECK ADD 
	CONSTRAINT [WODMailLog_PK] PRIMARY KEY  CLUSTERED 
	(
		[WODMailLogID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WODMailLog1] ON [dbo].[WODMailLog]([WOID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WODetail] (
	[WODetailID] [int] IDENTITY (1, 1) NOT NULL ,
	[WODetailTypeID] [int] NOT NULL ,
	[WOTypeID] [int] NOT NULL ,
	[WOID] [int] NOT NULL ,
	[DetailID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WODetail] WITH NOCHECK ADD 
	CONSTRAINT [WODetail_PK] PRIMARY KEY  CLUSTERED 
	(
		[WODetailID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WODetail1] ON [dbo].[WODetail]([WOID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WOHistory] (
	[WOHistoryID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOID] [int] NOT NULL ,
	[WOActionID] [int] NOT NULL ,
	[WOActionNote] [varchar] (200) NOT NULL ,
	[WOActionDate] [datetime] NOT NULL ,
	[WOActionUser] [varchar] (50) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOHistory] WITH NOCHECK ADD 
	CONSTRAINT [WOHistory_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOHistoryID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WOHistory1] ON [dbo].[WOHistory]([WOID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WONote] (
	[WONoteID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOID] [int] NOT NULL ,
	[WONote] [varchar] (250) NOT NULL ,
	[WONoteDate] [datetime] NOT NULL ,
	[WONoteUser] [varchar] (50) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WONote] WITH NOCHECK ADD 
	CONSTRAINT [WONote_PK] PRIMARY KEY  CLUSTERED 
	(
		[WONoteID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WONote1] ON [dbo].[WONote]([WOID]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WOPriority] (
	[WOPriorityID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOPriority] WITH NOCHECK ADD 
	CONSTRAINT [WOPriority_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOPriorityID]
	)  ON [PRIMARY] 
GO


CREATE TABLE [dbo].[WORelationship] (
	[WORelationshipID] [int] IDENTITY (1, 1) NOT NULL ,
	[WOIDLeft] [int] NOT NULL ,
	[WOIDRight] [int] NOT NULL ,
	[WORelationshipTypeID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WORelationship] WITH NOCHECK ADD 
	CONSTRAINT [WORelationship_PK] PRIMARY KEY  CLUSTERED 
	(
		[WORelationshipID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WORelationship1] ON [dbo].[WORelationship]([WOIDLeft]) ON [PRIMARY]
GO
CREATE  INDEX [IX_WORelationship2] ON [dbo].[WORelationship]([WOIDRight]) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WORelationshipType] (
	[WORelationshipTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WORelationshipType] WITH NOCHECK ADD 
	CONSTRAINT [WORelationshipType_PK] PRIMARY KEY  CLUSTERED 
	(
		[WORelationshipTypeID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[WOStatus] (
	[WOStatusID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOStatus] WITH NOCHECK ADD 
	CONSTRAINT [WOStatus_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOStatusID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[WOType] (
	[WOTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[WOPriorityIDDefault] [int] NOT NULL ,
	[WOStatusIDDefault] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOType] WITH NOCHECK ADD 
	CONSTRAINT [WOType_PK] PRIMARY KEY  CLUSTERED 
	(
		[WOTypeID]
	)  ON [PRIMARY] 
GO

CREATE TABLE [dbo].[WOTypeDetail] (
	[WODetailTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL ,
	[Description] [varchar] (100) NOT NULL ,
	[WOLocationTable] [varchar] (100) NOT NULL ,
	[WOTypeID] [int] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WOTypeDetail] WITH NOCHECK ADD 
	CONSTRAINT [WODetailType_PK] PRIMARY KEY  CLUSTERED 
	(
		[WODetailTypeID]
	)  ON [PRIMARY] 
GO
CREATE  INDEX [IX_WOTypeDetail1] ON [dbo].[WOTypeDetail]([WOTypeID]) ON [PRIMARY]
GO



/** FOREIGN KEYS **/

ALTER TABLE [dbo].[WO] ADD 
	CONSTRAINT [WOPriority_WO_FK1] FOREIGN KEY 
	(
		[WOPriorityID]
	) REFERENCES [dbo].[WOPriority] (
		[WOPriorityID]
	),
	CONSTRAINT [WOStatus_WO_FK1] FOREIGN KEY 
	(
		[WOStatusID]
	) REFERENCES [dbo].[WOStatus] (
		[WOStatusID]
	),
	CONSTRAINT [WOType_WO_FK1] FOREIGN KEY 
	(
		[WOTypeID]
	) REFERENCES [dbo].[WOType] (
		[WOTypeID]
	)
GO

ALTER TABLE [dbo].[WOActionPath] ADD 
	CONSTRAINT [WOAction_WOActionPath_FK1] FOREIGN KEY 
	(
		[WOActionID]
	) REFERENCES [dbo].[WOAction] (
		[WOActionID]
	),
	CONSTRAINT [WOType_WOActionPath_FK1] FOREIGN KEY 
	(
		[WOTypeID]
	) REFERENCES [dbo].[WOType] (
		[WOTypeID]
	)
GO

ALTER TABLE [dbo].[WOActionPathOptions] ADD 
	CONSTRAINT [WOType_WOActionPathOptions_FK1] FOREIGN KEY 
	(
		[WOTypeID]
	) REFERENCES [dbo].[WOType] (
		[WOTypeID]
	)
GO

ALTER TABLE [dbo].[WODFaxAttachmentLog] ADD 
	CONSTRAINT [WO_WODFaxAttachmentLog_FK1] FOREIGN KEY 
	(
		[WOID]
	) REFERENCES [dbo].[WO] (
		[WOID]
	),
	CONSTRAINT [WODFaxLog_WODFaxAttachmentLog_FK1] FOREIGN KEY 
	(
		[WODFaxLogID]
	) REFERENCES [dbo].[WODFaxLog] (
		[WODFaxLogID]
	),
	CONSTRAINT [WODFileLog_WODFaxAttachmentLog_FK1] FOREIGN KEY 
	(
		[WODFileLogID]
	) REFERENCES [dbo].[WODFileLog] (
		[WODFileLogID]
	)
GO

ALTER TABLE [dbo].[WODFileLog] ADD 
	CONSTRAINT [WO_WODFileLog_FK1] FOREIGN KEY 
	(
		[WOID]
	) REFERENCES [dbo].[WO] (
		[WOID]
	) ON DELETE CASCADE 
GO

ALTER TABLE [dbo].[WODMailAttachmentLog] ADD 
	CONSTRAINT [WO_WODMailAttachmentLog_FK1] FOREIGN KEY 
	(
		[WOID]
	) REFERENCES [dbo].[WO] (
		[WOID]
	),
	CONSTRAINT [WODFileLog_WODMailAttachmentLog_FK1] FOREIGN KEY 
	(
		[WODFileLogID]
	) REFERENCES [dbo].[WODFileLog] (
		[WODFileLogID]
	) ON DELETE CASCADE ,
	CONSTRAINT [WODMailLog_WODMailAttachmentLog_FK1] FOREIGN KEY 
	(
		[WODMailLogID]
	) REFERENCES [dbo].[WODMailLog] (
		[WODMailLogID]
	)
GO

ALTER TABLE [dbo].[WODMailLog] ADD 
	CONSTRAINT [WO_WODMailLog_FK1] FOREIGN KEY 
	(
		[WOID]
	) REFERENCES [dbo].[WO] (
		[WOID]
	) ON DELETE CASCADE 
GO

ALTER TABLE [dbo].[WODetail] ADD 
	CONSTRAINT [WO_WODetail_FK1] FOREIGN KEY 
	(
		[WOID]
	) REFERENCES [dbo].[WO] (
		[WOID]
	) ON DELETE CASCADE ,
	CONSTRAINT [WOType_WODetail_FK1] FOREIGN KEY 
	(
		[WOTypeID]
	) REFERENCES [dbo].[WOType] (
		[WOTypeID]
	),
	CONSTRAINT [WOTypeDetail_WODetail_FK1] FOREIGN KEY 
	(
		[WODetailTypeID]
	) REFERENCES [dbo].[WOTypeDetail] (
		[WODetailTypeID]
	)
GO

ALTER TABLE [dbo].[WOHistory] ADD 
	CONSTRAINT [WO_WOHistory_FK1] FOREIGN KEY 
	(
		[WOID]
	) REFERENCES [dbo].[WO] (
		[WOID]
	) ON DELETE CASCADE ,
	CONSTRAINT [WOAction_WOHistory_FK1] FOREIGN KEY 
	(
		[WOActionID]
	) REFERENCES [dbo].[WOAction] (
		[WOActionID]
	)
GO

ALTER TABLE [dbo].[WONote] ADD 
	CONSTRAINT [WO_WONote_FK1] FOREIGN KEY 
	(
		[WOID]
	) REFERENCES [dbo].[WO] (
		[WOID]
	)
GO

ALTER TABLE [dbo].[WORelationship] ADD 
	CONSTRAINT [WO_WORelationship_FK1] FOREIGN KEY 
	(
		[WOIDLeft]
	) REFERENCES [dbo].[WO] (
		[WOID]
	) ON DELETE CASCADE ,
	CONSTRAINT [WORelationshipType_WORelationship_FK1] FOREIGN KEY 
	(
		[WORelationshipTypeID]
	) REFERENCES [dbo].[WORelationshipType] (
		[WORelationshipTypeID]
	)
GO

ALTER TABLE [dbo].[WOType] ADD 
	CONSTRAINT [WOPriority_WOType_FK1] FOREIGN KEY 
	(
		[WOPriorityIDDefault]
	) REFERENCES [dbo].[WOPriority] (
		[WOPriorityID]
	),
	CONSTRAINT [WOStatus_WOType_FK1] FOREIGN KEY 
	(
		[WOStatusIDDefault]
	) REFERENCES [dbo].[WOStatus] (
		[WOStatusID]
	)
GO

ALTER TABLE [dbo].[WOTypeDetail] ADD 
	CONSTRAINT [WOType_WODetailType_FK1] FOREIGN KEY 
	(
		[WOTypeID]
	) REFERENCES [dbo].[WOType] (
		[WOTypeID]
	)
GO

/** SCRIPT COMPLETE **/