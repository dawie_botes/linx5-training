if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFErrorLog_insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFErrorLog_insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFFileSetting_delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFFileSetting_delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFGeneralSetting_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFGeneralSetting_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFHTTPSetting_update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFHTTPSetting_update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessInvoke_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessInvoke_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessInvoke_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessInvoke_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessLogParameter_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessLogParameter_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessLog_Archive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessLog_Archive]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessLog_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessLog_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessLog_LINX_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessLog_LINX_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessParameter_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessParameter_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcessesView_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcessesView_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_CFLinxProcesses_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_CFLinxProcesses_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Commom_EndSubProcess]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Commom_EndSubProcess]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Dashboard_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Dashboard_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Dashboard_Status]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Dashboard_Status]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_ErrorLog_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_ErrorLog_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_ExecuteLinxSql]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_ExecuteLinxSql]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_ExecuteSQL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_ExecuteSQL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_Detail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_Detail]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DetailEditNextAction]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DetailEditNextAction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DetailEditPriority]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DetailEditPriority]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DetailEditSource]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DetailEditSource]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DetailEditStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DetailEditStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DetailRelations]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DetailRelations]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DetailType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DetailType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_DetailTypeSelect]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_DetailTypeSelect]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_History]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_History]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_InsertNote]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_InsertNote]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_FollowUp_Notes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_FollowUp_Notes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_GetCFSourceFileFormat]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_GetCFSourceFileFormat]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_GetExceptionReport_Columns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_GetExceptionReport_Columns]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_GetLinxProcessDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_GetLinxProcessDetails]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_GetTableColumnList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_GetTableColumnList]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_LINX_sysobjects_select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_LINX_sysobjects_select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_LinxProcesses_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_LinxProcesses_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_MaintenanceConfigurationFileSettings_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_MaintenanceConfigurationFileSettings_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_MaintenanceConfigurationFileSettings_View]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_MaintenanceConfigurationFileSettings_View]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_MaintenanceConfigurationMailSettings_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_MaintenanceConfigurationMailSettings_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Search_DataGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Search_DataGrid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Search_NextAction]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Search_NextAction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Search_Priority]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Search_Priority]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Search_Source]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Search_Source]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Search_Status]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Search_Status]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_Search_WOType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_Search_WOType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_UpdateNextActionSeq]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_UpdateNextActionSeq]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_UpdateWOFromWODetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_UpdateWOFromWODetail]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOActionComplete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOActionComplete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOActionError]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOActionError]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOActionPathOptions_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOActionPathOptions_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOActionPathOptions_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOActionPathOptions_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOActionPath_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOActionPath_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOActionPath_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOActionPath_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOAction_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOAction_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOAction_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOAction_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOArchiveSetting_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOArchiveSetting_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOArchiveSetting_Select1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOArchiveSetting_Select1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOArchive_Move]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOArchive_Move]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOArchive_UpdateField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOArchive_UpdateField]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOArchive_UpdateField1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOArchive_UpdateField1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOCreate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOCreate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODFaxLog_insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODFaxLog_insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODFaxLog_select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODFaxLog_select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODFileLog_insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODFileLog_insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODFileLog_update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODFileLog_update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODFileLog_update1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODFileLog_update1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODFileLog_update2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODFileLog_update2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODMailAttachmentLog_insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODMailAttachmentLog_insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODMailLog_Classify]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODMailLog_Classify]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODMailLog_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODMailLog_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WODetail_insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WODetail_insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOHistory_insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOHistory_insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOHistory_insert1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOHistory_insert1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WORelationship_insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WORelationship_insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WORelationship_insert_1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WORelationship_insert_1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOTypeDetail_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOTypeDetail_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOTypeDetail_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOTypeDetail_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOType_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOType_Delete]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WOType_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WOType_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WO_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WO_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WO_Insert_1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WO_Insert_1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WO_WOHistory_update1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WO_WOHistory_update1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[prc_WO_update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[prc_WO_update]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO













CREATE PROCEDURE dbo.prc_CFErrorLog_insert
/**
**AUTHOR		: Johan Khn
**DATE CREATED	:
**PURPOSE		: This storproc inserts a new record in the CfErrorLog table. 
**NOTES		:
**

**/

	(@ErrorMessage_1 	[varchar](5000),
	 @ProcessName_2 	[varchar](100),
	 @EventName_3 	[varchar](100),
	 @ErrorTimeStamp_4 	[varchar](100),
	 @WOID_5 		[int])

AS INSERT INTO [CFErrorLog] 
	 ( [ErrorMessage],
	 [ProcessName],
	 [EventName],
	 [ErrorTimeStamp],
	 [WOID]) 
 
VALUES 
	( SUBSTRING (@ErrorMessage_1,1,2000) ,
	 @ProcessName_2,
	 @EventName_3,
	 CASE WHEN ISDATE(@ErrorTimeStamp_4) = 0 THEN CURRENT_TIMESTAMP ELSE @ErrorTimeStamp_4 END,
	 @WOID_5)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE [dbo].[prc_CFFileSetting_delete]

	@CFFileSettingID int

AS

	DELETE FROM CFSourceFileFormat
	WHERE	TableId = (SELECT ID FROM CFSourceTable WHERE CFFileSettingID = @CFFileSettingID)

	DELETE FROM CFSourceTable
	WHERE		CFFileSettingID = @CFFileSettingID

	DELETE FROM CFFileSetting
	WHERE		CFFileSettingID = @CFFileSettingID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE [dbo].[prc_CFGeneralSetting_Update]

	@CFGeneralSettingID int = null,
	@SettingValue int = null, 
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS


UPDATE dbo.CFGeneralSetting 
SET	
	SettingValue = @SettingValue
WHERE
	CFGeneralSettingID = @CFGeneralSettingID

SET @CFGeneralSettingID = ISNULL(SCOPE_IDENTITY(), -1)

IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Update Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Update Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO










CREATE PROCEDURE [dbo].[prc_CFHTTPSetting_update]
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2004/03/15
**PURPOSE		: This storproc updates CFHTTPSetting with last run date and  param values used. 
**NOTES			:
**
**/

	(@CFHTTPSettingID 	[int],
	 @ParamValuePrevious	[varchar] (500)
	 )


AS 
UPDATE dbo.CFHTTPSetting 
SET  PreviousRunDate = CURRENT_TIMESTAMP 
WHERE 
	CFHTTPSettingID	 = @CFHTTPSettingID

UPDATE dbo.CFHTTPParamSetting 
SET  ParamValuePrevious	 = @ParamValuePrevious
WHERE 
	CFHTTPSettingID	 = @CFHTTPSettingID

SELECT @@ROWCOUNT AS RowCountReturned,  @@ERROR	AS ErrorReturned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO











CREATE     PROCEDURE [dbo].[prc_CFLinxProcessInvoke_Insert]
/**
**AUTHOR		: Christiaan Pretorius (Digiata)
**DATE CREATED		: 2005-06-09
**PURPOSE		: This stored proc inserts a new record in the CFLinxProcessInvoke table. This table is used to enable invoking Linx Processes from Stadium with Parameters
**NOTES		:
**/
	(
		@LinxProcessName	varchar(150),		
		@UserName		varchar(150) = null
	)
AS
SET NOCOUNT ON

	INSERT INTO CFLinxProcessInvoke
		(LinxProcessName, UserName, UpdatedOn, InvokeComplete)
	VALUES
		(@LinxProcessName, @UserName, GETDATE(), 0)

return @@IDENTITY











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO









CREATE   PROCEDURE [dbo].[prc_CFLinxProcessInvoke_Update]
/**
**AUTHOR		: Christiaan Pretorius (Digiata)
**DATE CREATED		: 2005-09-19
**PURPOSE		: This stored proc updates the Invoke State of the CFLinxProcessInvoke table. This table is used to enable invoking Linx Processes from Stadium with Parameters
**NOTES		:
**/
	(
		@CFLinxProcessInvokeID	int,		
		@InvokeComplete		int
	)
AS


	UPDATE CFLinxProcessInvoke
		SET InvokeComplete = cast(@InvokeComplete as bit)
	WHERE CFLinxProcessInvokeID = @CFLinxProcessInvokeID













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO







CREATE      PROCEDURE [dbo].[prc_CFLinxProcessLogParameter_Insert]
/**
**AUTHOR		: Joel Meyerowitz
**DATE CREATED		: 2005-09-20
**PURPOSE		: This stored proc inserts a new record in the CFLinxProcessLogParameter table. This table is used to enable invoking Linx Processes from Stadium with Parameters
**NOTES		:
**/
	(
		@LinxProcessID		int,		
		@CaptureUser		varchar(150),
		@CFLinxProcessLogID	int
	)
AS
SET NOCOUNT ON

	INSERT INTO CFLinxProcessLogParameter(CFLinxProcessID,CFLinxProcessLogID,ParameterValue)
	VALUES(@LinxProcessID,@CFLinxProcessLogID,@CaptureUser)

RETURN @@IDENTITY







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE dbo.prc_CFLinxProcessLog_Archive
(
	@WOIDArchiveAgeLimit 			int,	
	@KeepAtLeastNumberOfEntries 		int)
AS
/**
**AUTHOR		: Christiaan Pretorius (Digiata)
**DATE CREATED		: 2006-04-13
**PURPOSE		: This stored proc archives the Linx process log
**NOTES			: You can specify both a minimum age as well as number of records to keep
			: If you are not interested in either, then send that item though as 0
			: For instance, if you want to keep the last 10 records, regardless of how old they are, then send age of 0 through, atleast = 10
			: Or, if you want to keep the last 10 days' worth, regalrdless of how many, then send age = 10, and atleast = 0
			: Or, a combination - the last 30 days, but at least 100 records: age 30, atleast = 100
			:2006/05/29 - jm - Changed Store Proc to output parameters used to update the archive history.
**/

--Declare Output Parameters
DECLARE @Success AS Integer,@RecordCount AS Integer,@MinID AS Integer,@MaxID AS Integer

CREATE TABLE #CFLinxProcessLogID_Delete(CFLinxProcessLogID INT)
CREATE TABLE #CFLinxProcessID_Delete(CFLinxProcessID INT)
INSERT INTO #CFLinxProcessID_Delete 
	SELECT CFLinxProcessID 
	FROM CFLinxProcessLog	
	GROUP BY CFLinxProcessID
	HAVING Count(*) > @KeepAtLeastNumberOfEntries 

INSERT INTO #CFLinxProcessLogID_Delete
	SELECT lpl.CFLinxProcessLogID FROM CFLinxProcessLog lpl INNER JOIN #CFLinxProcessID_Delete lpd
	ON lpl.CFLinxProcessID = lpd.CFLinxProcessID
	WHERE DateDiff(day,CurrentDate,GetDate()) > @WOIDArchiveAgeLimit

--SEt Output Parameters
SELECT @MinID = MIN(CFLinxProcessLogID) ,@MaxID=MAX(CFLinxProcessLogID),@RecordCount = COUNT(CFLinxProcessLogID) FROM #CFLinxProcessLogID_Delete

DELETE FROM CFLinxProcessLogParameter WHERE CFLinxProcessLogID IN (SELECT CFLinxProcessLogID FROM #CFLinxProcessLogID_Delete)
DELETE FROM CFLinxProcessLog WHERE CFLinxProcessLogID IN (SELECT CFLinxProcessLogID FROM #CFLinxProcessLogID_Delete)

IF @@Error <> 0 goto ErrorHandler


DROP TABLE #CFLinxProcessLogID_Delete
DROP TABLE #CFLinxProcessID_Delete

IF @@Error = 0
BEGIN
SET @Success = 1
END

SELECT @MinID AS MinTableKey,@MaxID AS MaxTableKey,@RecordCount AS UpdateCount,@Success AS Success

ErrorHandler:
	DROP TABLE #CFLinxProcessLogID_Delete
	DROP TABLE #CFLinxProcessID_Delete 

	DECLARE @Message VARCHAR(500)
	SET @Message 	= 'Error ' + convert(varchar,@@Error) 
			+ ' [prc_CFLinxProcessLog_Archive @WOArchiveSettingID = ' --+ convert(varchar,@WOArchiveSettingID)
			+ ' @RunDate = ' + convert(varchar(11), getdate(), 106)
	RAISERROR (@Message,16,1)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO






CREATE  PROCEDURE [dbo].[prc_CFLinxProcessLog_Insert]
/**
**AUTHOR		: Joel Meyerowitz
**DATE CREATED		: 2005-09-20
**PURPOSE		: This stored proc inserts a new record in the CFLinxProcessLog table. This table is used to enable invoking Linx Processes from Stadium with Parameters
**NOTES		:
**/
	(
		@LinxProcessID		int,		
		@StatusID		int,
		@Notes			varchar(100),
		@ReturnID		int OUTPUT		
	)
AS
SET NOCOUNT ON
	INSERT INTO CFLinxProcessLog
		(CFLinxProcessID,CurrentDate,CFLinxProcessStatusID, Notes)
	VALUES
		(@LinxProcessID, GETDATE(),@StatusID, @Notes)

SET @ReturnID = @@IDENTITY

SELECT @ReturnID





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE [dbo].[prc_CFLinxProcessLog_LINX_Insert]
/**
**AUTHOR		: Joel Meyerowitz
**DATE CREATED		: 2005-09-20
**PURPOSE		: This stored proc inserts a new record in the CFLinxProcessLog and CFLinxProcessLogParameter tables.
**NOTES			: NR - Altered this stor proc to checks if the process that it was called by exist. If it does not then a new record is inserted into the CFLinxProcess table.
**/
	(
		@LinxProcess		varchar(100),		
		@StatusID		int,
		@Notes			varchar(100)
	)
AS
SET NOCOUNT ON
	
DECLARE @LogID AS Integer, @LinxProcessID int

if exists (SELECT Process FROM CFLinxProcess WHERE Process like @LinxProcess) 
BEGIN
	SELECT @LinxProcessID = CfLinxProcessID
	FROM CfLinxProcess
	WHERE Process like @LinxProcess

	EXECUTE prc_CFLinxProcessLog_Insert @LinxProcessID, @StatusID, @Notes, @LogID OUTPUT

	EXECUTE prc_CFLinxProcessLogParameter_Insert @LinxProcessID,'Linx',@LogID
END
ELSE
BEGIN
	INSERT INTO cflinxprocess (Process, ProcessActive, ProcessInvoke, SortSeq)
	VALUES			  (@LinxProcess, 0, 0, 0)

	EXECUTE prc_CFLinxProcessLog_Insert @@IDENTITY, @StatusID, @Notes, @LogID OUTPUT

	EXECUTE prc_CFLinxProcessLogParameter_Insert @@IDENTITY,'Linx',@LogID
END


RETURN @@IDENTITY






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE  PROCEDURE [dbo].[prc_CFLinxProcessParameter_Insert]
/**
**AUTHOR		: Christiaan Pretorius (Digiata)
**DATE CREATED		: 2005-06-09
**PURPOSE		: This stored proc inserts a new record in the CFLinxProcessParameter table. This table is used to enable invoking Linx Processes from Stadium with Parameters
**NOTES		:
**/
	(
		@CFLinxProcessInvokeID	int,
		@ParameterName		varchar(50),
		@ParamType		varchar(50),
		@ParameterValue		varchar(500),
		@Direction		varchar(10)
	)
AS
SET NOCOUNT ON
	INSERT INTO CFLinxProcessParameter
		(CFLinxProcessInvokeID, ParameterName, ParamType, ParameterValue, Direction)
	VALUES
		(@CFLinxProcessInvokeID, @ParameterName, @ParamType, @ParameterValue, @Direction)

SELECT @@IDENTITY AS CFLinxProcessParameterID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO









CREATE PROCEDURE dbo.prc_CFLinxProcessesView_DataGrid (@ID int)
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/23
**PURPOSE	: Populates the datagrid on the LINX Processes View screen in Stadium
**NOTES		: 
**/
AS

	SELECT DISTINCT CFLinxProcess.CFLinxProcessID AS ID,CFLinxProcessLog.CFLinxProcessLogID, 
			CFLinxProcessLog.CurrentDate, CFLinxProcessLogStatus.Status,CFLinxProcessLog.Notes, 
			CFLinxProcessLogParameter.ParameterValue AS [User] 
	FROM CFLinxProcess 
	LEFT OUTER JOIN CFlinxProcessLog ON 
	CFLinxProcess.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID 
	INNER JOIN CFlinxProcessLogStatus ON 
	CFlinxProcessLogStatus.CFlinxProcessLogStatusID = CFlinxProcessLog.CFlinxProcessStatusID 
	INNER JOIN CFLinxProcessLogParameter ON 
	CFLinxProcessLogParameter.CFLinxProcessLogID = CFLinxProcessLog.CFLinxProcessLogID 
	WHERE CFLinxProcess.CFLinxProcessID = @ID
	AND (DATEDIFF(d, GETDATE(), CFLinxProcessLog.CurrentDate) > - 7)
	ORDER BY CFLinxProcessLog.CFLinxProcessLogID desc





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_CFLinxProcesses_DataGrid
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/23
**PURPOSE	: Populates the datagrid on the LINX Processes screen in Stadium
**NOTES		: 
**/
AS

	SELECT DISTINCT CFLinxProcess.CFLinxProcessID AS ID,CFLinxProcess.Process AS [Process Name],
	CFLinxProcessLog.CurrentDate, CFLinxProcessLogStatus.Status,CFLinxProcessLog.Notes,
	CFLinxProcessLogParameter.ParameterValue AS [User],[Description], LINXServer AS [LINX Server],
	Protocol,Port,ServerUser,ServerPassword,SolutionFile,Project,CFLinxProcess.SortSeq,
	CFLinxProcess.ProcessInvoke 
	FROM CFLinxProcess 
	INNER JOIN CFlinxProcessLog ON 
	CFLinxProcess.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID 
	INNER JOIN CFlinxProcessLogStatus ON 
	CFlinxProcessLogStatus.CFlinxProcessLogStatusID = CFlinxProcessLog.CFlinxProcessStatusID 
	INNER JOIN CFLinxProcessLogParameter ON 
	CFLinxProcessLogParameter.CFLinxProcessLogID = CFLinxProcessLog.CFLinxProcessLogID
	WHERE CFLinxProcessLog.CFLinxProcessLogID IN 	(SELECT MAX(CFLinxProcessLogID) 
							FROM CFLinxProcessLog 
							INNER JOIN CFLinxProcess 
							ON CFLinxProcess.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID  
							GROUP BY CFLinxProcess.CFLinxProcessID)
	AND (CFLinxProcess.ProcessActive = 1) 
	UNION
	SELECT DISTINCT CFLinxProcess.CFLinxProcessID AS ID,CFLinxProcess.Process AS [Process Name],'', '','','',
	[Description], LINXServer AS [LINX Server],Protocol,Port,ServerUser,ServerPassword,SolutionFile,Project,
	CFLinxProcess.SortSeq,CFLinxProcess.ProcessInvoke
	FROM CFLinxProcess
	WHERE CFLinxProcess.CFLinxProcessID NOT IN 	(SELECT CFLinxProcessID 
							FROM CFLinxProcessLog  
							GROUP BY CFLinxProcessID)
	AND (CFLinxProcess.ProcessActive = 1) 
	ORDER BY CFLinxProcess.SortSeq







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



CREATE PROCEDURE [dbo].[prc_Commom_EndSubProcess]
(
	@BatchID INT,
	@SubProcessName VARCHAR(200)
)
AS

IF EXISTS
(
	SELECT 
		*
	FROM
		WO
	INNER JOIN
		WODSubProcessActive ON WO.WOID = WODSubProcessActive.BatchID
	INNER JOIN 
		WODSubProcessOrder ON WODSubProcessActive.SubProcessName = WODSubProcessOrder.[Name]
	WHERE
		WO.NextActionSequence <> 115
		and WODSubProcessActive.BatchID = @BatchID
)
BEGIN
	UPDATE 
		WODSubProcessActive
	SET
		SubProcessName = @SubProcessName
	WHERE
		BatchID = @BatchID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO












CREATE     PROCEDURE dbo.prc_Dashboard_DataGrid
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/11/30
**PURPOSE	: Populates the datagrid on the Dashboard screen in Stadium
**NOTES		: DP - 2005/12/28: changed the stored proc to use hash tables intead od table variables, Original code commented out below
		: JK - 2006/04/25: changed process groups to cater for Nedbank processes
**/
AS


CREATE TABLE #NextRunTimeSlots
	(
	CFLinxProcessID int NULL,
	NextRunTime datetime NULL
	)  


CREATE TABLE #StartDate
	(
	CFLinxProcessID int NULL,
	currentDate datetime NULL
	)  


CREATE TABLE #EndDate
	(
	CFLinxProcessID int NULL,
	currentDate datetime NULL
	)  


CREATE TABLE #CFLinxProcessLogID
	(
	CFLinxProcessLogID int NULL
	)  


DECLARE @RunTime DateTime
SELECT @RunTime = GETDATE()

INSERT INTO #NextRunTimeSlots
SELECT
	CFLinxProcessID, 
	CASE 
		WHEN Interval IS NULL THEN Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime) 
		WHEN @RunTime <= Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime) THEN Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime)
		WHEN @RunTime > Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + EndTime) THEN Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime)
		WHEN 
			@Runtime >= DateAdd(second,Interval * (datediff(second,Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime),@RunTime) / Interval),Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime)) Then
				DateAdd(second,Interval * (1+(datediff(second,Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime),@RunTime) / Interval)),Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime))
			Else
				DateAdd(second,Interval * (datediff(second,Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime),@RunTime) / Interval),Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime))

	END As NextRunTime
	
FROM 
	CFNextRunTime

INSERT INTO #StartDate
	select CFLinxProcessID, currentDate
		FROM CFLinxProcessLog (NOLOCK)
		WHERE cflinxprocesslogid IN (
		SELECT MAX(CFLinxProcessLogID)
		FROM CFLinxProcessLog (NOLOCK)
		WHERE CFLinxProcessStatusID = 1
		Group By CFLinxProcessID)

INSERT INTO #EndDate
	select CFLinxProcessID, currentdate
		FROM CFLinxProcessLog (NOLOCK)
		WHERE cflinxprocesslogid IN (
		SELECT MAX(CFLinxProcessLogID)
		FROM CFLinxProcessLog (NOLOCK)
		WHERE CFLinxProcessStatusID = 3
		Group By CFLinxProcessID)


INSERT INTO #CFLinxProcessLogID
SELECT MAX(CFLinxProcessLogID) AS CFLinxProcessLogID
						FROM CFLinxProcessLog (NOLOCK)
						INNER JOIN CFLinxProcess (NOLOCK)
						ON CFLinxProcess.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID  
						GROUP BY CFLinxProcess.CFLinxProcessID

SELECT DISTINCT 
	CFLinxProcess.CFLinxProcessID AS ID, 
	CFLinxProcess.Process AS [Process Name],
	CFLinxProcessLog.CurrentDate, 
	CFLinxProcessLogStatus.Status,
	CFLinxProcessLog.Notes, 
	CFLinxProcessLogParameter.ParameterValue AS [User],
	CFLinxProcess.[Description], 
	CFLinxProcessLog.CFLinxProcessStatusID,	
	CONVERT(Varchar(20),nrt.NextRunTime,20) AS NextRunTime,
	'' as Heading,
	case When (DateDiff(second,tblDuration.StartDate, tblDuration.EndDate)) < 0 Then 
		NULL 
	Else 
		DateDiff(second,tblDuration.StartDate, tblDuration.EndDate) End 
	AS Duration
	
FROM 
	CFLinxProcess (NOLOCK) 
	INNER JOIN CFlinxProcessLog (NOLOCK) ON CFLinxProcess.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID 
	INNER JOIN CFlinxProcessLogStatus (NOLOCK) ON CFlinxProcessLogStatus.CFlinxProcessLogStatusID = CFlinxProcessLog.CFlinxProcessStatusID 
	INNER JOIN CFLinxProcessLogParameter (NOLOCK) ON CFLinxProcessLogParameter.CFLinxProcessLogID = CFLinxProcessLog.CFLinxProcessLogID 
	LEFT JOIN (SELECT CFLinxProcessID, Min(NextRunTime) NextRunTime
		   FROM	#NextRunTimeSlots (NOLOCK) 
	    	   WHERE NextRunTime >= @RunTime
		   GROUP BY CFLinxProcessID ) nrt ON nrt.CFLinxProcessID = CFLinxProcess.CFLinxProcessID 
	LEFT JOIN (SELECT s.CFLinxProcessID, s.currentdate as StartDate, e.currentdate as EndDate
		   FROM #StartDate s (NOLOCK) LEFT JOIN #EndDate e (NOLOCK) ON s.CFLinxProcessID = e.CFLinxProcessID) tblDuration ON tblDuration.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID 

WHERE CFLinxProcessLog.CFLinxProcessLogID IN 	(SELECT DISTINCT CFLinxProcessLogID
						FROM #CFLinxProcessLogID (NOLOCK))
	
	
	UNION
	SELECT DISTINCT CFLinxProcess.CFLinxProcessID AS ID,CFLinxProcess.Process AS [Process Name],'', '','','',
	[Description], 0, '', '', ''
	FROM CFLinxProcess (NOLOCK)
	WHERE CFLinxProcess.CFLinxProcessID NOT IN 	(SELECT CFLinxProcessID 
							FROM CFLinxProcessLog  (NOLOCK)
							GROUP BY CFLinxProcessID)
	UNION
	SELECT '', 'Common', null, null, null, null, 'Common processes that are invoked by LINX', 0, null, '<B><SPAN style="FONT-SIZE: 8pt; COLOR: blue; FONT-FAMILY: Arial">Common Processes</SPAN></FONT></B>', null
	UNION
	SELECT '', 'Group', null, null, null, null, 'Groups that can be invoked from the Invoke Linx Process screen', 0, null, '<B><SPAN style="FONT-SIZE: 8pt; COLOR: blue; FONT-FAMILY: Arial">Grouped Processes</SPAN></FONT></B>', null
	UNION
	SELECT '', 'T1 Processes', null, null, null, null, 'Source Data Load', 0, null, '<B><SPAN style="FONT-SIZE: 8pt; COLOR: blue; FONT-FAMILY: Arial">Source Data Load</SPAN></FONT></B>', null
	UNION
	SELECT '', 'T2 Processes', null, null, null, null, 'Target Data Upload', 0, null, '<B><SPAN style="FONT-SIZE: 8pt; COLOR: blue; FONT-FAMILY: Arial">Target Data Upload</SPAN></FONT></B>', null
	UNION
	SELECT '', 'T3 Processes', null, null, null, null, 'Recon Source to Target', 0, null, '<B><SPAN style="FONT-SIZE: 8pt; COLOR: blue; FONT-FAMILY: Arial">Recon Source to Target</SPAN></FONT></B>', null
	
	ORDER BY CFLinxProcess.Process, CFLinxProcessLog.CurrentDate desc


DROP TABLE #NextRunTimeSlots
DROP TABLE #StartDate
DROP TABLE #enddate
DROP TABLE #CFLinxProcessLogID


/**

SELECT
	0 AS ID, 
	'Under Construction' [Process Name],
	GETDATE() CurrentDate, 
	NULL Status,
	NULL Notes, 
	NULL [User],
	NULL [Description], 
	NULL CFLinxProcessStatusID,	
	NULL NextRunTime,
	'' as Heading,
	NULL Duration




DECLARE @NextRunTimeSlots TABLE (CFLinxProcessID INT, NextRunTime DateTime)
DECLARE @StartDate TABLE (CFLinxProcessID INT, currentDate DateTime)
DECLARE @EndDate TABLE (CFLinxProcessID INT, currentDate DateTime)

DECLARE @RunTime DateTime
SELECT @RunTime = GETDATE()

INSERT INTO @NextRunTimeSlots
SELECT
	CFLinxProcessID, 
	CASE 
		WHEN Interval IS NULL THEN Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime) 
		WHEN @RunTime <= Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime) THEN Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime)
		WHEN @RunTime > Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + EndTime) THEN Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime)
		WHEN 
			@Runtime >= DateAdd(second,Interval * (datediff(second,Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime),@RunTime) / Interval),Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime)) Then
				DateAdd(second,Interval * (1+(datediff(second,Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime),@RunTime) / Interval)),Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime))
			Else
				DateAdd(second,Interval * (datediff(second,Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime),@RunTime) / Interval),Convert(datetime,convert(varchar(10),@RunTime,23) + space(1) + StartTime))

	END As NextRunTime
	
FROM 
	CFNextRunTime

INSERT INTO @StartDate
	select CFLinxProcessID, currentDate
		from CFLinxProcessLog (NOLOCK)
		where cflinxprocesslogid IN (
		SELECT MAX(CFLinxProcessLogID)
		From CFLinxProcessLog (NOLOCK)
		Where CFLinxProcessStatusID = 1
		Group By CFLinxProcessID)

INSERT INTO @EndDate
	select CFLinxProcessID, currentdate
		from CFLinxProcessLog (NOLOCK)
		where cflinxprocesslogid IN (
		SELECT MAX(CFLinxProcessLogID)
		From CFLinxProcessLog (NOLOCK)
		Where CFLinxProcessStatusID = 3
		Group By CFLinxProcessID)




SELECT DISTINCT 
	CFLinxProcess.CFLinxProcessID AS ID, 
	CFLinxProcess.Process AS [Process Name],
	CFLinxProcessLog.CurrentDate, 
	CFLinxProcessLogStatus.Status,
	CFLinxProcessLog.Notes, 
	CFLinxProcessLogParameter.ParameterValue AS [User],
	CFLinxProcess.[Description], 
	CFLinxProcessLog.CFLinxProcessStatusID,	
	CONVERT(Varchar(20),nrt.NextRunTime,20) as NextRunTime,
	'' as Heading,
	case When (DateDiff(second,Duration.StartDate, Duration.EndDate)) < 0 Then 
		NULL 
	Else 
		DateDiff(second,Duration.StartDate, Duration.EndDate) End 
	As Duration
	
FROM 
	CFLinxProcess (NOLOCK) INNER JOIN 
	CFlinxProcessLog (NOLOCK) ON CFLinxProcess.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID INNER JOIN 
	CFlinxProcessLogStatus (NOLOCK) ON CFlinxProcessLogStatus.CFlinxProcessLogStatusID = CFlinxProcessLog.CFlinxProcessStatusID INNER JOIN 
	CFLinxProcessLogParameter (NOLOCK) ON CFLinxProcessLogParameter.CFLinxProcessLogID = CFLinxProcessLog.CFLinxProcessLogID LEFT JOIN 
	(SELECT CFLinxProcessID, Min(NextRunTime) NextRunTime
		FROM
			@NextRunTimeSlots
		WHERE NextRunTime >= @RunTime
		GROUP BY CFLinxProcessID ) nrt ON nrt.CFLinxProcessID = CFLinxProcess.CFLinxProcessID LEFT JOIN 
	(SELECT s.CFLinxProcessID, s.currentdate as StartDate, e.currentdate as EndDate
		FROM @StartDate s LEFT JOIN @EndDate e ON s.CFLinxProcessID = e.CFLinxProcessID) Duration ON Duration.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID 
	

	WHERE CFLinxProcessLog.CFLinxProcessLogID IN 	(SELECT MAX(CFLinxProcessLogID) 
							FROM CFLinxProcessLog (NOLOCK)
							INNER JOIN CFLinxProcess 
							ON CFLinxProcess.CFLinxProcessID = CFLinxProcessLog.CFLinxProcessID  
							GROUP BY CFLinxProcess.CFLinxProcessID)
	
	
	UNION
	SELECT DISTINCT CFLinxProcess.CFLinxProcessID AS ID,CFLinxProcess.Process AS [Process Name],'', '','','',
	[Description], 0, '', '', ''
	FROM CFLinxProcess (NOLOCK)
	WHERE CFLinxProcess.CFLinxProcessID NOT IN 	(SELECT CFLinxProcessID 
							FROM CFLinxProcessLog  (NOLOCK)
							GROUP BY CFLinxProcessID)
	UNION
	SELECT '', 'Common', null, null, null, null, 'Common processes that are invoked by LINX', 0, null, 'Common', null
	UNION
	SELECT '', 'Group', null, null, null, null, 'Groups that can be invoked from the Invoke Linx Process screen', 0, null, 'Group', null
	UNION
	SELECT '', 'T1 Processes', null, null, null, null, 'Instruction processes', 0, null, 'T1 Processes', null
	UNION
	SELECT '', 'T2 Processes', null, null, null, null, 'Settlement processes', 0, null, 'T2 Processes', null
	UNION
	SELECT '', 'T3 Processes', null, null, null, null, 'Confirmation processes', 0, null, 'T3 Processes', null
	UNION
	SELECT '', 'T6 Processes', null, null, null, null, 'Distribution processes', 0, null, 'T6 Processes', null
	UNION
	SELECT '', 'T7 Processes', null, null, null, null, 'Rebate processes', 0, null, 'T7 Processes', null

	ORDER BY CFLinxProcess.Process, CFLinxProcessLog.CurrentDate desc
**/

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO







CREATE PROCEDURE dbo.prc_Dashboard_Status
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/11/24
**PURPOSE	: Populates the Status combobox for the Dashboard screen in stadium
**NOTES		: 
**/
AS
	SELECT Status
	FROM CFLinxProcessLogStatus
	WHERE CFLinxProcessLogStatusID <> 2








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO









CREATE PROCEDURE dbo.prc_ErrorLog_DataGrid
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Popluate the data grid on the Error Log screen in stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 
		TOP 100 PERCENT dbo.CFErrorLog.CFErrorLogID, dbo.CFErrorLog.ErrorMessage, dbo.CFErrorLog.ProcessName, 
		dbo.CFErrorLog.EventName, dbo.CFErrorLog.ErrorTimeStamp, 
		dbo.CFErrorLog.WOID, dbo.WOType.Name AS WOType, dbo.WOStatus.Name AS WOStatus
	FROM
		dbo.WOType INNER JOIN
		dbo.WO ON dbo.WOType.WOTypeID = dbo.WO.WOTypeID INNER JOIN
		dbo.WOStatus ON dbo.WO.WOStatusID = dbo.WOStatus.WOStatusID INNER JOIN
		dbo.WOPriority ON dbo.WO.WOPriorityID = dbo.WOPriority.WOPriorityID RIGHT OUTER JOIN
		dbo.CFErrorLog ON dbo.WO.WOID = dbo.CFErrorLog.WOID
	ORDER BY 
		dbo.CFErrorLog.CFErrorLogID DESC







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE PROCEDURE [dbo].[prc_ExecuteLinxSql]
(
	@Command VARCHAR(4000)
)

AS

DECLARE @NewCommand NVARCHAR(4000)

EXEC sp_ExecuteSql @NewCommand

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

create procedure [dbo].[prc_ExecuteSQL]
	@SQL varchar(8000)
as

	execute (@SQL)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE dbo.prc_FollowUp_DataGrid
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the datagrid for the follow-up screen in stadium
**NOTES		: 
**/
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT  	WO.WOID, 
				WOType.Name AS Type,
				WO.WOTypeID,
				WO.WOStatusID,
				WOStatus.Name AS Status, 
				WOPriority.Name AS Priority, 
				WO.WOPriorityID,
				WO.NextActionDate,
				WOAction.Name AS [Next Action],
				WO.NextActionSequence,CFInterfaceType.Name as Source,WOActionPath.ActionOperator
	FROM         	WOPriority INNER JOIN
				WO ON WOPriority.WOPriorityID = WO.WOPriorityID INNER JOIN
				WOStatus ON WO.WOStatusID = WOStatus.WOStatusID INNER JOIN
				WOType ON WO.WOTypeID = WOType.WOTypeID INNER JOIN
				WOActionPath INNER JOIN
				WOAction ON WOActionPath.WOActionID = WOAction.WOActionID ON 
				WO.NextActionSequence = WOActionPath.ActionSequence AND WO.WOTypeID = WOActionPath.WOTypeID INNER JOIN
				CFInterfaceType ON WO.WOSource = CFInterfaceType.CFInterfaceTypeID
	WHERE     	(WOActionPath.ActionOperator = 'MANUAL')
	ORDER BY 	WO.WOID DESC, WO.WOTypeID, WO.NextActionSequence, WOPriority.WOPriorityID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_FollowUp_Detail
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Gets the details on a particular WOID and displays it in the Work Order screen in stadium
**NOTES		: 
**/
	(@WOID 	[int])

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT    	WO.WOID, 
				WOType.Name AS WOType, 
				WOStatus.Name AS Status, 
				WOPriority.Name AS Priority, 
				WO.NextActionDate,
				WO.WOPriorityID,
				WO.WOStatusID,
				WO.WOTypeID,
				WO.NextActionSequence,
				WOAction.Name AS NextAction,
				WOActionPath.ActionOperator
	FROM         	WOPriority INNER JOIN
				WO ON WOPriority.WOPriorityID = WO.WOPriorityID INNER JOIN
				WOStatus ON WO.WOStatusID = WOStatus.WOStatusID INNER JOIN
				WOType ON WO.WOTypeID = WOType.WOTypeID INNER JOIN
				WOActionPath INNER JOIN
				WOAction ON WOActionPath.WOActionID = WOAction.WOActionID ON 
				WO.NextActionSequence = WOActionPath.ActionSequence AND WO.WOTypeID = WOActionPath.WOTypeID INNER JOIN
				CFInterfaceType ON WO.WOSource = CFInterfaceType.CFInterfaceTypeID
	WHERE 		WOID = @WOID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_FollowUp_DetailEditNextAction
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Next Action combobox for the Work Order Detail - Edit screen in stadium
**NOTES		: 
**/
	(@WOTypeID int,
	 @NextActionSequence int,
	 @WOID int)

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT  	WA.Name as PossibleNextAction,
				WOPO.PossibleNextActionSequence
	FROM 		WOActionPathOptions WOPO,
				WOAction WA,
				WOActionPath WOP,
				WO
	WHERE 		WA.WOActionID = WOP.WOActionID
	AND 		WOP.WOTypeID = WOPO.WOTypeID
	AND 		WOPO.PossibleNextActionSequence = WOP.ActionSequence
	AND		WO.NextActionSequence = WOPO.ActionSequence
	AND 		WO.WOTypeID = WOPO.WOTypeID
	AND 		WOPO.WOTypeID = @WOTypeID
	AND 		WOPO.ActionSequence = @NextActionSequence
	AND 		WO.WOID = @WOID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_FollowUp_DetailEditPriority
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Priority combobox for the Work Order Detail - Edit screen in stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT WOPriorityID, Name 
	FROM WOPriority







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO









CREATE PROCEDURE dbo.prc_FollowUp_DetailEditSource
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Source combobox for the Work Order Detail - Edit screen in stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	CFInterfaceTypeID,Name
	FROM 	CFInterfaceType
	WHERE 	CFInterfaceTypeID in (1,2)







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_FollowUp_DetailEditStatus
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Status combobox for the Work Order Detail - Edit screen in stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT WOStatusID, Name 
	FROM WOStatus







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO












CREATE PROCEDURE dbo.prc_FollowUp_DetailRelations
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the WORelations datagrid on the WO Detail screen in stadium
**NOTES		: 
**/
	(@WOTypeID int,
	 @NextActionSequence int,
	 @WOID int)

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	
		WORelationshipID, @NextActionSequence as  NextActionSequence , 
		@WOTypeID  as WOTypeID , 
		WORelationship.WOIDLeft AS [Related Parent WO's],   
		WOType.Name AS [Parent WO  Type],   
		0 AS [Related Child WO's],   
		'' AS [Child WO Type]  
	FROM
	   	WORelationship 	INNER JOIN 
		WORelationshipType ON WORelationship.WORelationshipTypeID = WORelationshipType.WORelationshipTypeID 
		INNER JOIN WO ON WORelationship.WOIDLeft = WO.WOID
		INNER JOIN WO WO_1 ON WORelationship.WOIDRight = WO_1.WOID 
		INNER JOIN WOType ON WO.WOTypeID = WOType.WOTypeID
		INNER JOIN WOType WOType_1 ON WO_1.WOTypeID = WOType_1.WOTypeID  
	WHERE
	   	WOIDRight = @WOID
	UNION  
	SELECT  	WORelationshipID, @NextActionSequence as  NextActionSequence , 
				@WOTypeID  as WOTypeID ,  
				0 AS [Related Parent WO's],   
				'' AS [Parent WO  Type],   
				WORelationship.WOIDRight AS [Related Child WO's],   
				WOType_1.Name AS [Child WO Type]   
	FROM    	WORelationship 
				INNER JOIN WORelationshipType ON WORelationship.WORelationshipTypeID = WORelationshipType.WORelationshipTypeID 
				INNER JOIN WO ON WORelationship.WOIDLeft = WO.WOID 
				INNER JOIN WO WO_1 ON WORelationship.WOIDRight = WO_1.WOID 
				INNER JOIN WOType ON WO.WOTypeID = WOType.WOTypeID 
				INNER JOIN WOType WOType_1 ON WO_1.WOTypeID = WOType_1.WOTypeID  
	WHERE 		WOIDLeft = @WOID
	ORDER BY 	WORelationship.WOIDLeft DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.prc_FollowUp_DetailType
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the WOType datagrid on the WO Detail screen in stadium
**NOTES		: 
**/
	(@WOTypeID int,
	 @WOID int)

AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	
		@WOID AS WOID, WODetailTypeID, [Name], WOLocationTable, WOTypeID  
	FROM 		
		WOTypeDetail
	WHERE
		WOTypeID = @WOTypeID
		AND	WOLocationTable in (SELECT [name] FROM SYSOBJECTS WHERE TYPE in('U','V'))
		AND WODetailTypeID IN (SELECT WODetailTypeID FROM WODETAIL WHERE WOID = @WOID AND WOTYPEID = @WOTypeID)

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO









CREATE PROCEDURE dbo.prc_FollowUp_DetailTypeSelect
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: This stor proc determince which WOType detail to display in the grid WO Detail screen in stadium
**NOTES		: 
**/
	(@WOTypeID int,
	 @WOID int,
	 @WOLocationTable varchar(100))

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	DECLARE @SQL nvarchar(4000)
	SELECT @SQL = 'SELECT  ' + cast(@WOTypeID as varchar(50)) + ' AS WOTypeID, * FROM ' + Cast(@WOLocationTable as varchar(100)) + ' WHERE WOID = ' + Cast(@WOID as varchar(50))
	EXEC( @SQL )






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_FollowUp_History
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Gets the history on a particular WOID and displays it in the Work Order History screen in stadium
**NOTES		: 
**/
	(@WOID 	[int])

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	WOID, WOActionNote, WOActionDate, WOActionUser
	FROM	WOHistory
	WHERE	WOID= @WOID
	ORDER BY WOActionDate







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_FollowUp_InsertNote
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: This stor proc inserts a note into the WONote Table
**NOTES		: 
**/
	(@WOID int,
	 @WONote varchar(50),
	 @WONoteUser varchar(50))

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	INSERT INTO WONote(WOID, WONote, WONoteDate, WONoteUser) 
	VALUES (@WOID, @WONOTE, CURRENT_TIMESTAMP, @WONoteUser)







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO









CREATE PROCEDURE dbo.prc_FollowUp_Notes
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Gets the notes for a particular WOID and displays it in the Notes on Work Order screen in stadium
**NOTES		: 
**/
	(@WOID 	[int])

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT WONoteID, WOID, WONote, WONoteUser, WONoteDate 
	FROM WONote 
	WHERE WOID = @WOID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

create procedure [dbo].[prc_GetCFSourceFileFormat]
	@FileType varchar(50)
as

declare @LikeFileType varchar(50)
set @LikeFileType = '%' + @FileType + '%'

select	t.TableName, t.CFFileSettingID, f.DataElement, f.DataType, f.StartPosition, f.Length
from	CFSourceTable t inner join CFSourceFileFormat f on (t.[ID] = f.TableID)
where	t.TableName like @LikeFileType
order by f.[id]

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE PROCEDURE [dbo].[prc_GetExceptionReport_Columns] 
(
	@FileSettingID INT
)
AS

select 
	syscolumns.name
from 
	sysobjects
inner join
	syscolumns on sysobjects.ID = syscolumns.ID
inner join
	CFFileSetting on sysobjects.name = CFFileSetting.Name
where
	CFFileSetting.CFFileSettingID = @FileSettingID
	and syscolumns.name <> 'ID'
order by
	syscolumns.colorder

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO












CREATE   PROCEDURE dbo.prc_GetLinxProcessDetails (
/**
**AUTHOR		: Vilen Muruvan
**DATE CREATED	: 2005/02/17
**PURPOSE		: Retrieve the details of a LINX process details for  use in Stadium
**NOTES		: 
**/
	@LinxProcessID int,
	@Protocol varchar(50) = null OUTPUT, 
	@Port varchar(50) = null OUTPUT, 
	@LINXServer varchar(50) = null OUTPUT,
	@ServerUser varchar(50) = null OUTPUT,
	@ServerPassword varchar(50) = null OUTPUT,
	@SolutionFile varchar(250) = null OUTPUT,
	@Project varchar(100) = null OUTPUT,
	@Process varchar(100) = null OUTPUT
)

AS

SELECT	@Protocol = Protocol, @Port = Port, @LINXServer = LINXServer, @ServerUser = ServerUser, 
	 	@ServerPassword = ServerPassword, @SolutionFile = SolutionFile, @Project = Project, @Process = Process
FROM 		CFLinxProcess
WHERE 	CFLinxProcessID = @LinxProcessID











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



create procedure [dbo].[prc_GetTableColumnList]
	@TableName varchar(50), 
	@ColumnList nvarchar(4000) output
/************************************************************************************************************
**AUTHOR	: AD
**DATE CREATED	: 21-sep-05
**PURPOSE	: Returns a comma delimited list of the column names for the given table
**NOTES		:
************************************************************************************************************/
as
	declare @SQL nvarchar(4000), @ParamDefinition nvarchar(200), @ColumnName varchar(200)
begin
set nocount on

	create table #TableColumns (col varchar(200))

	set @ParamDefinition = '@TableNameIn nvarchar(200)'
	set @SQL = 	'insert into #TableColumns
			select 	c.[name]
			from 	sysobjects o inner join syscolumns c on (o.[id] = c.[id])
			where 	o.[name] = @TableNameIn 
			order by c.colorder' 
	execute sp_executesql @SQL, @ParamDefinition, @TableNameIn = @TableName

	set @ColumnList = ''
	declare cols cursor fast_forward for
		select 	col 
		from	#TableColumns

	open cols
	fetch next from cols into @ColumnName
	while (@@FETCH_STATUS = 0)
	begin
		set @ColumnList = @ColumnList + '[' + isnull(@ColumnName,'') + '],'
		fetch next from cols into @ColumnName	
	end 
	close cols
	deallocate cols

	set @ColumnList = substring(@ColumnList, 1, len(@ColumnList) - 1)
	drop table #TableColumns

set nocount off
end	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE [dbo].[prc_LINX_sysobjects_select]
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2003/10/09
**PURPOSE		: This storproc select a list of user tables. Used for testing
**NOTES		:
**
**/
	(@Type 		[varchar](200)) -- should be 'U' for user tables

AS 
SELECT name FROM sysobjects
WHERE type = @Type 
--AND SUBSTRING(name,1,3)='WOD'
ORDER BY name










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_LinxProcesses_DataGrid
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/23
**PURPOSE	: Populates the datagrid on the LinxAdmin - Invoke Linx Process screen in Stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT  TOP 100 PERCENT CFLinxProcessID, Name AS [Process Name], Description, LINXServer AS [LINX Server],
		Protocol, Port,	ServerUser,	ServerPassword,	SolutionFile, SolutionName,	Project, Process
	FROM	dbo.CFLinxProcess
	WHERE	(ProcessActive = 1) AND (ProcessInvoke = 1)
	ORDER BY SortSeq, CFLinxProcessID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



CREATE PROCEDURE [dbo].[prc_MaintenanceConfigurationFileSettings_DataGrid]
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/23
**PURPOSE	: Populates the datagrid on the File Settings screen in Stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT     	CFFileSetting.CFFileSettingID AS [File Setting ID], CFTradingPartner.Name AS Partner, CFSystem.Name AS System, 
			CFFormat.Name AS Format, CFFileSetting.Name AS [File Setting Name], CFFileSetting.Description AS Description, 
			CFFileSetting.FileName AS [File Name], CFFileSetting.FileLocation AS [File Location], 
			CFFileSetting.BackupFileName AS [Backup File Name], CFFileSetting.BackupFileLocation AS [Backup File Location], 
			CFFileSetting.ConstraintAlwaysCreated AS [Always Avail.], CFFileSetting.ConstraintFreq AS [Freq.], 
			CFFileSetting.ContraintFromTime AS [Avail. From], CFFileSetting.ContraintToTime AS [Avail. To]
	FROM         	CFFileSetting 
	INNER JOIN	CFTradingPartner ON CFFileSetting.CFTradingPartnerID = CFTradingPartner.CFTradingPartnerID
	INNER JOIN	CFSystem ON CFFileSetting.CFSystemID = CFSystem.CFSystemID
	INNER JOIN	CFFormat ON CFFileSetting.CFFormatID = CFFormat.CFFormatID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_MaintenanceConfigurationFileSettings_View
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/23
**PURPOSE	: Populates the datagrid on the File Settings screen in Stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT     	CFFileSetting.CFFileSettingID AS [File Setting ID], CFTradingPartner.Name AS Partner, CFSystem.Name AS System, 
			CFFormat.Name AS Format, CFFileSetting.Name AS [File Setting Name], CFFileSetting.Description AS Description, 
			CFFileSetting.FileName AS [File Name], CFFileSetting.FileLocation AS [File Location], 
			CFFileSetting.BackupFileName AS [Backup File Name], CFFileSetting.BackupFileLocation AS [Backup File Location], 
			CFFileSetting.ConstraintAlwaysCreated AS [Always Avail.], CFFileSetting.ConstraintFreq AS [Freq.], 
			CFFileSetting.ContraintFromTime AS [Avail. From], CFFileSetting.ContraintToTime AS [Avail. To]
	FROM         	CFFileSetting
	INNER JOIN	CFTradingPartner ON CFFileSetting.CFTradingPartnerID = CFTradingPartner.CFTradingPartnerID 
	INNER JOIN	CFSystem ON CFFileSetting.CFSystemID = CFSystem.CFSystemID
	INNER JOIN	CFFormat ON CFFileSetting.CFFormatID = CFFormat.CFFormatID







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_MaintenanceConfigurationMailSettings_DataGrid
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/23
**PURPOSE	: Populates the datagrid on the Mail Settings screen in Stadium
**NOTES		: 
**/

AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT  CFMailSetting.CFMailSettingID AS [Mail Setting ID], CFMailSetting.CFTradingPartnerID AS [Partner ID], 
		CFTradingPartner.Name AS [Partner Group], CFMailSetting.Name AS Name, CFMailSetting.Description AS Description, 
		CFMailSetting.MReceiver AS [Receiver Address], CFMailSetting.MCC AS [CC Address], CFMailSetting.MBCC AS [BCC Address], 
		CFMailSetting.MSubject AS [Message Subject], CFMailSetting.MBody AS [Message Body], CFMailSetting.MSender as [From Address]
	FROM    CFMailSetting INNER JOIN
		CFTradingPartner ON CFMailSetting.CFTradingPartnerID = CFTradingPartner.CFTradingPartnerID
				







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_Search_DataGrid
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the datagrid for the search screen in stadium
**NOTES		: 
**/
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT  	WO.WOID, 
				WOType.Name AS WOType, 
				WOStatus.Name AS Status, 
				WOPriority.Name AS Priority, 
				WO.WOStatusID,
				WO.WOPriorityID,
				WO.NextActionSequence,
				WO.WOTypeID,
			WOAction.Name AS NextAction, 
				WO.NextActionDate,
				CFInterfaceType.Name AS Source,
				WOActionPath.WOActionID as NextActionID,WOActionPath.ActionOperator
	FROM         	dbo.WOPriority INNER JOIN
				WO ON WOPriority.WOPriorityID = WO.WOPriorityID INNER JOIN
				WOStatus ON WO.WOStatusID = WOStatus.WOStatusID INNER JOIN
				WOType ON WO.WOTypeID = WOType.WOTypeID INNER JOIN
				WOActionPath INNER JOIN
				WOAction ON WOActionPath.WOActionID = WOAction.WOActionID ON 
				WO.NextActionSequence = WOActionPath.ActionSequence AND WO.WOTypeID = WOActionPath.WOTypeID INNER JOIN
				CFInterfaceType ON WO.WOSource = CFInterfaceType.CFInterfaceTypeID
	ORDER BY 	WO.WOID DESC







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO









CREATE PROCEDURE dbo.prc_Search_NextAction
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Next Action combobox for the search screen in stadium
**NOTES		: 
**/
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	distinct [Name],min(WOActionId) as WOActionID
	FROM		WOAction
	GROUP BY	[Name]







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_Search_Priority
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Priority combobox for the search screen in stadium
**NOTES		: 
**/
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	distinct [Name],min(WOPriorityID) as WOPriorityID  
	FROM 		WOPriority
	GROUP BY 	[Name]







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_Search_Source
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Source combobox for the search screen in stadium
**NOTES		: 
**/
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	distinct [Name] 
	FROM CFInterfaceType







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_Search_Status
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the Status combobox for the search screen in stadium
**NOTES		: 
**/
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	distinct [Name],min(WOStatusID) as WOStatusID  
	FROM		WOStatus
	GROUP BY 	[Name]







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO








CREATE PROCEDURE dbo.prc_Search_WOType
/**
**AUTHOR	: Nicholas Riva (Digiata)
**DATE CREATED	: 2005/09/22
**PURPOSE	: Populates the WOType combobox for the search screen in stadium
**NOTES		: 
**/
AS
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	SELECT 	distinct [Name], min(WOTypeID) as WOTypeID 
	FROM 		WOType 
	GROUP BY	[Name]







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO











create Procedure dbo.prc_UpdateNextActionSeq
/**
**AUTHOR	: Tessa Marais
**DATE CREATED	: 2003/10/28
**PURPOSE	: Update WO.NextActionSequence 
**NOTES		:	
**
**/

(
@StatusId int,
@NextAction int,
@WOID INT)
AS
UPDATE WO
SET WO.NEXTACTIONSEQUENCE = @NextAction,
WO.WOStatusID=@StatusId,
WO.NextActionDate = getdate()
where WO.WOID=@WOID











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE  PROCEDURE [dbo].[prc_UpdateWOFromWODetail]
(

/**
**AUTHOR		: Tessa Marais
**DATE CREATED	:	/2003/09/15
**PURPOSE		: Update the WO from Stadium and inserts into history
**NOTES		:	
**

**/

@WOID INT,
@NextActionSeq int,
@Status int,
@Priority int,
@User varchar(50))
AS
UPDATE WO
SET NextActionSequence=@NextActionSeq,
NextActionDate=GETDATE(),
WOStatusID=@Status,
WOPriorityID=@Priority
WHERE WOID = @WOID

declare @strsql varchar(200)
select @strsql = 'NextActionSeq=' + cast(@NextActionSeq as varchar(10)) + '  StatusID=' + cast(@Status as varchar(2)) + '  Priority=' + cast(@Priority as varchar(2))

insert into WOHistory(WOID,WOActionID,WOActionNote,WOActionDate,WOActionUser)
values(@WOID,49,@strsql,getdate(),@User)









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE procedure [dbo].[prc_WOActionComplete]
	@WOID int,
	@WOAction varchar(100),
	@User varchar(100)
/*
	1. Log the WOAction in the WOHistory table
	2. Update the NextActionSequence for the WOID in WO
*/
as
	declare @WOActionID int
begin
	select @WOActionID = WOActionID from WOAction where [Name] = @WOAction

	insert into WOHistory (WOID, WOActionID, WOActionNote, WOActionDate, WOActionUser)
	values (@WOID, @WOActionID, 'Action Completed: ' + @WOAction, getdate(), @User)

	update	WO 	 
	set	WO.NextActionSequence = [dbo].[GetActionSequenceNext](WOType.[Name], @WOAction),
		WO.NextActionDate = getdate(),
		WO.WOStatusID = (select WOStatusID from WOStatus where [Name] = 'Active')
	from	WO 	inner join WOType on (WO.WOTypeID = WOType.WOTypeID)
	where	WO.WOID = @WOID
end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO





create procedure [dbo].[prc_WOActionError]
	@WOID int,
	@WOAction varchar(50),
	@User varchar(50),
	@ErrorAction varchar(50)
/*
	1. Log that error occurred with the given WOAction in the WOHistory table
	2. Update the WOID status to rejected and the WOID next action sequence
	   to the sequence of the given error action
*/
as
	declare @WOActionID int
begin
	select @WOActionID = WOActionID from WOAction where [Name] = @WOAction

	insert into WOHistory (WOID, WOActionID, WOActionNote, WOActionDate, WOActionUser)
	values (@WOID, @WOActionID, 'Error in action: ' + @WOAction, getdate(), @User)

	update	WO
	set	WO.NextActionSequence = WOActionPath.ActionSequence,
		WO.WOStatusID = (select WOStatusID from WOStatus where [Name] = 'Rejected')
	from	WO 	inner join WOType on (WO.WOTypeID = WOType.WOTypeID)
			inner join WOActionPath on (WOType.WOTypeID = WOActionPath.WOTypeID)
			inner join WOAction on (WOAction.WOActionID = WOActionPath.WOActionID
						and WOAction.[Name] = @ErrorAction)
	where	WO.WOID = @WOID

end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


CREATE PROCEDURE dbo.prc_WOActionPathOptions_Delete
	@WOActionPathOptionsID int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/
DELETE FROM dbo.WOActionPathOptions
WHERE
	WOActionPathOptionsID = @WOActionPathOptionsID


IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Delete Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Delete Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



CREATE PROCEDURE dbo.prc_WOActionPathOptions_Update

	@WOActionPathOptionsID int = null ,
	@WOTypeID int = null, 
	@ActionSequence int = null, 
	@PossibleNextActionSequence int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/

UPDATE dbo.WOActionPathOptions 
SET	
	WOTypeID = CASE WHEN @WOTypeID IS NOT NULL THEN @WOTypeID ELSE WOTypeID END,
	ActionSequence = CASE WHEN @ActionSequence IS NOT NULL THEN @ActionSequence ELSE ActionSequence END,
	PossibleNextActionSequence = CASE WHEN @PossibleNextActionSequence IS NOT NULL THEN @PossibleNextActionSequence ELSE PossibleNextActionSequence END
WHERE
	WOActionPathOptionsID = @WOActionPathOptionsID


SET @WOActionPathOptionsID = ISNULL(SCOPE_IDENTITY(), -1)

IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Update Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Update Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


CREATE PROCEDURE dbo.prc_WOActionPath_Delete
	@WOActionPathID int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/
DELETE FROM dbo.WOActionPath
WHERE
	WOActionPathID = @WOActionPathID


IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Delete Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Delete Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



CREATE PROCEDURE dbo.prc_WOActionPath_Update

	@WOActionPathID int = null ,
	@WOTypeID int = null, 
	@WOActionID int = null, 
	@ActionRequired char(1) = null, 
	@ActionOperator varchar(50) = null, 
	@ActionSequence int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/

UPDATE dbo.WOActionPath 
SET	
	WOTypeID = CASE WHEN @WOTypeID IS NOT NULL THEN @WOTypeID ELSE WOTypeID END,
	WOActionID = CASE WHEN @WOActionID IS NOT NULL THEN @WOActionID ELSE WOActionID END,
	ActionRequired = CASE WHEN @ActionRequired IS NOT NULL THEN @ActionRequired ELSE ActionRequired END,
	ActionOperator = CASE WHEN @ActionOperator IS NOT NULL THEN @ActionOperator ELSE ActionOperator END,
	ActionSequence = CASE WHEN @ActionSequence IS NOT NULL THEN @ActionSequence ELSE ActionSequence END
WHERE
	WOActionPathID = @WOActionPathID


SET @WOActionPathID = ISNULL(SCOPE_IDENTITY(), -1)

IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Update Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Update Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


CREATE PROCEDURE dbo.prc_WOAction_Delete
	@WOActionID int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/
DELETE FROM dbo.WOAction
WHERE
	WOActionID = @WOActionID


IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Delete Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Delete Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



CREATE PROCEDURE dbo.prc_WOAction_Update

	@WOActionID int = null ,
	@Name varchar(50) = null, 
	@Description varchar(100) = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/

UPDATE dbo.WOAction 
SET	
	Name = CASE WHEN @Name IS NOT NULL THEN @Name ELSE Name END,
	Description = CASE WHEN @Description IS NOT NULL THEN @Description ELSE Description END
WHERE
	WOActionID = @WOActionID


SET @WOActionID = ISNULL(SCOPE_IDENTITY(), -1)

IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Update Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Update Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



/*===================================================================================================================*/
CREATE procedure [dbo].[prc_WOArchiveSetting_Select]
/*
**AUTHOR	: Albertus Dunn
**DATE CREATED	: 2005/10/12
**PURPOSE	: Returns the list of active archive processes that must run
**NOTES		:
*/
as

begin
set nocount on

	select	WOArchiveSettingID, WOArchiveTypeID, WOTypeID, WODTable, WODTableField,
		WODTableWOIDField, WODTableKeyField, WOIDArchiveAgeLimit, ArchiveTargetTable
	from 	WOArchiveSetting
	where	Active = 1
	order by ArchiveSequence

set nocount off
end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




CREATE  PROCEDURE [dbo].[prc_WOArchiveSetting_Select1] 
/**
**AUTHOR	: Johan Khn
**DATE CREATED	: 2006/08/05
**PURPOSE	: This storproc selects the archive setting to be run in the archive process
**NOTES		:
**		: 2006/08/05 - copy of the original setting selection from 2005/07/12. Created as a new sp.
**/
AS

SET NOCOUNT ON
DECLARE 
@WOArchiveSettingID int, @WOArchiveTypeID int, @WOTypeID int, @WODTable varchar(200), @WODTableField varchar(200), @WODTableWOIDField varchar(200), 
@WODTableKeyField varchar(200), @CFFileSettingsID int, @WOIDArchiveAgeLimit int, @ArchiveTargetTable varchar(200), @ArchiveSequence int, @Active int, 
@1stRun int, @WOIDStart int, @WOIDEnd int, @WODTableKeyStart int, @WODTableKeyEnd int,
@SQL nvarchar(2000), @ParamDefinition nvarchar(200)


DECLARE @ResultTable TABLE
(WOArchiveSettingID int, WOArchiveTypeID int, WOTypeID int, WODTable varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS, WODTableField varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS, WODTableWOIDField varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS, 
WODTableKeyField varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS, CFFileSettingsID int, WOIDArchiveAgeLimit int, ArchiveTargetTable varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS, ArchiveSequence int, Active int, 
[1stRun] int, WOIDStart int, WOIDEnd int, WODTableKeyStart int, WODTableKeyEnd int)

	
DECLARE ArchiveSetting CURSOR FOR

SELECT     TOP 100 PERCENT AS1.WOArchiveSettingID, AS1.WOArchiveTypeID, AS1.WOTypeID, AS1.WODTable , AS1.WODTableField, AS1.WODTableWOIDField, 
                      AS1.WODTableKeyField, AS1.CFFileSettingsID, AS1.WOIDArchiveAgeLimit, AS1.ArchiveTargetTable, AS1.ArchiveSequence, AS1.Active, 
                      ISNULL(AH1.WODArchiveHistoryID, 0) AS [1stRun], -- =0 means ThereWasNoPreviousRun, >0 means ThereWasAPreviousRun
ISNULL(AH1.WOIDEnd,0) AS WOIDStart, --PreviousEnd - IF NULL then 0
ISNULL(AH1.WOIDEnd,0) AS WOIDEnd, -- New Selected end according to WOHistory
ISNULL(AH1.WODTableKeyEnd,0) AS WODTableKeyStart, --PreviousEnd - IF NULL then 0
ISNULL(AH1.WODTableKeyEnd,0) AS WODTableKeyEnd -- New Selected end according to WOHistory and Table ID,

FROM         dbo.WOArchiveSetting AS1 LEFT OUTER JOIN
                          (SELECT     AH.WODArchiveHistoryID, AH.WOID, AH.WOArchiveSettingID, AH.WOIDStart, AH.WOIDEnd, AH.WODTableKeyStart, AH.WODTableKeyEnd,
                                                    AH.RunUser, AH.RunDate, AH.RunDescription
                            FROM          dbo.WODSrcArchiveHistory AH INNER JOIN
                                                       (SELECT     MAX(WODArchiveHistoryID) AS WODArchiveHistoryID_Max, WOArchiveSettingID
                                                         FROM          WODSrcArchiveHistory
				    WHERE RunDescription LIKE '%Success = 1%'
                                                         GROUP BY WOArchiveSettingID) A ON AH.WOArchiveSettingID = A.WOArchiveSettingID AND 
                                                   AH.WODArchiveHistoryID = A.WODArchiveHistoryID_Max) AH1 ON AS1.WOArchiveSettingID = AH1.WOArchiveSettingID
WHERE     (AS1.Active = 1) 
AND AS1.WOArchiveTypeID IN(1,2,3,4)
ORDER BY AS1.ArchiveSequence


OPEN ArchiveSetting
FETCH NEXT FROM  ArchiveSetting 
--Select into variables
INTO @WOArchiveSettingID , @WOArchiveTypeID , @WOTypeID , @WODTable , @WODTableField , @WODTableWOIDField , 
@WODTableKeyField , @CFFileSettingsID , @WOIDArchiveAgeLimit , @ArchiveTargetTable , @ArchiveSequence , @Active , 
@1stRun , @WOIDStart , @WOIDEnd , @WODTableKeyStart , @WODTableKeyEnd 

WHILE @@FETCH_STATUS = 0
BEGIN
--Set new Variable Values for WOIDEnd and WODTableKeyEnd
	--Set the WOIDEnd value
	SET @ParamDefinition = '@ReplaceOUT int OUTPUT'
	SET @SQL = 'SELECT @ReplaceOUT =  MAX(WOID) 
	FROM         dbo.WO
	WHERE     (WOTypeID = '+CONVERT(nvarchar,@WOTypeID)+') 
		AND (NextActionDate < DATEADD(d,-' + CONVERT(nvarchar,@WOIDArchiveAgeLimit) + ' , GETDATE())) 
		AND (NextActionSequence = 1000)' 
	EXECUTE sp_executesql @SQL, @ParamDefinition, @ReplaceOUT = @WOIDEnd OUTPUT

--Need to build different SQL for Normal move and WOHistoryMove
	IF @WOArchiveTypeID = 3
	BEGIN
	--Set the @WODTableKeyEnd value using WOIDEnd value from above
	SET @ParamDefinition = '@ReplaceOUT int OUTPUT'
	SET @SQL = 'SELECT @ReplaceOUT =  MAX('+@WODTableKeyField+') 
	FROM         dbo.'+@WODTable+' INNER JOIN WO ON WO.WOID= dbo.'+@WODTable+'.'+@WODTableWOIDField+'
	WHERE    '+@WODTable+'.'+@WODTableWOIDField+ '<='+CONVERT(nvarchar,@WOIDEnd)+'
	AND WO.WOTypeID = '+CONVERT(nvarchar,@WOTypeID)+' '
	END

	IF @WOArchiveTypeID <>3
	BEGIN
	--Set the @WODTableKeyEnd value using WOIDEnd value from above
	SET @ParamDefinition = '@ReplaceOUT int OUTPUT'
	SET @SQL = 'SELECT @ReplaceOUT =  MAX('+@WODTableKeyField+') 
	FROM         dbo.'+@WODTable+'
	WHERE    '+@WODTableWOIDField+ '<='+CONVERT(nvarchar,@WOIDEnd)
	END

	EXECUTE sp_executesql @SQL, @ParamDefinition, @ReplaceOUT = @WODTableKeyEnd OUTPUT

--Select the new updated variables
	INSERT INTO @ResultTable
	SELECT @WOArchiveSettingID AS WOArchiveSettingID, @WOArchiveTypeID AS WOArchiveTypeID, @WOTypeID AS WOTypeID, @WODTable AS WODTable, @WODTableField AS WODTableField, @WODTableWOIDField AS WODTableWOIDField, 
	@WODTableKeyField AS WODTableKeyField, @CFFileSettingsID AS CFFileSettingsID, @WOIDArchiveAgeLimit AS WOIDArchiveAgeLimit, @ArchiveTargetTable AS ArchiveTargetTable, @ArchiveSequence AS ArchiveSequence, @Active AS Active, 
	@1stRun AS [1stRun], @WOIDStart AS WOIDStart, ISNULL(@WOIDEnd,@WOIDStart) AS WOIDEnd, @WODTableKeyStart AS WODTableKeyStart, ISNULL(@WODTableKeyEnd,@WODTableKeyStart) AS WODTableKeyEnd
	


   FETCH NEXT FROM ArchiveSetting
	INTO @WOArchiveSettingID , @WOArchiveTypeID , @WOTypeID , @WODTable , @WODTableField , @WODTableWOIDField , 
	@WODTableKeyField , @CFFileSettingsID , @WOIDArchiveAgeLimit , @ArchiveTargetTable , @ArchiveSequence , @Active , 
	@1stRun , @WOIDStart , @WOIDEnd , @WODTableKeyStart , @WODTableKeyEnd 
END
CLOSE ArchiveSetting
DEALLOCATE ArchiveSetting

--Normal Settings
SELECT  R.*, 0 AS KeepAtLeastNumberOfEntries FROM @ResultTable R
--CFProcessLogSettings
UNION
SELECT     TOP 100 PERCENT AS1.WOArchiveSettingID, AS1.WOArchiveTypeID, AS1.WOTypeID, AS1.WODTable , AS1.WODTableField, AS1.WODTableWOIDField, 
                      AS1.WODTableKeyField, AS1.CFFileSettingsID, AS1.WOIDArchiveAgeLimit, AS1.ArchiveTargetTable, AS1.ArchiveSequence, AS1.Active, 
                      0 AS [1stRun], 0 AS WOIDStart,0 AS WOIDEnd, 0 AS WODTableKeyStart, 0 AS WODTableKeyEnd, AS1.KeepAtLeastNumberOfEntries
FROM         dbo.WOArchiveSetting AS1
WHERE     (AS1.Active = 1) 
AND AS1.WOArchiveTypeID =5

ORDER BY ArchiveSequence, WOArchiveTypeID, WOTypeID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




/*===================================================================================================================*/
CREATE   PROCEDURE [dbo].[prc_WOArchive_Move] 
/**
**AUTHOR	: Johan Khn
**DATE CREATED	: 2005/07/12
**PURPOSE	: This storproc moves records from a source table to a an archive table.
**NOTES		: 2005/09/20 AD - Change to handle new archive type 'Record Move History'
**		: 2005/10/12 - AD - Fix bug in type 3 Record Move History
**		: 2006/04/28 - JK - Added a new filter for the Transfer count. To cater for Type 4 (move with filter) where status of records have been changed after a first archive has run across the data.
		: 2006/08/10 - DP - Removed Fiter for selecting records where NextActionSequence = 1000
**	
**/
	@WOArchiveSettingID int,
	@WOArchiveTypeID int, 
	@WOTypeID int, 
	@WODTable varchar(100), 
	@WODTableField varchar(100),
	@WODTableWOIDField varchar(10), 
	@WODTableKeyField varchar(100), 
	@WOIDArchiveAgeLimit int,
	@ArchiveTargetTable varchar(100),
	@RunUser varchar(100),
	@RunDate datetime
AS

BEGIN
set nocount on

	declare @SQL nvarchar(4000), @ParamDefinition nvarchar(200), @SrcRecordCount int,
		@TrgInsertRowCount int, @SrcDeleteRowCount int, @Success int, @ErrorNr int,
		@TableFields varchar(2000), @WODTableFieldType varchar(50), @WOArchiveTypeFilter nvarchar(500),
		@WOIDCount int, @MinWOID int, @MaxWOID int, @MinTableKey int, @MaxTableKey int
		
	set @Success = 0

	-- create the filter for archive type 3 and 4
	set @WOArchiveTypeFilter = ''
	if @WOArchiveTypeID = 3
	  begin
		set @WOArchiveTypeFilter = ' and t.WOActionNote <> ''Records were archived'''
	  end
	if @WOArchiveTypeID = 4
	  begin
		select	@WODTableFieldType = t.[name]
		from	sysobjects o 	inner join syscolumns c on (o.[id] = c.[id])
					inner join systypes t on (c.xtype = t.xtype)
		where	o.[name] = @WODTable and c.[name] = @WODTableField

		set @WOArchiveTypeFilter =  
			' and t.' + @WODTableField 
			+ ' in (	select 	convert(' + @WODTableFieldType + ',FieldValue)
					from 	WOArchiveFilter 
					where 	WOArchiveSettingID = ' + convert(varchar, @WOArchiveSettingID)
			+ ' )' 
	  end

	declare @DateBefore varchar(11)
	select 	@DateBefore = convert(varchar(11), dateadd(day, -@WOIDArchiveAgeLimit, @RunDate))

	--extract WOID's
	create table #WOIDS (WOID int)
	set @ParamDefinition = '@ReplaceErrorOut int OUTPUT'
	set @SQL = 	'insert into #WOIDs select distinct t.' +   @WODTableWOIDField + ' 
			from 	dbo.WO w inner join dbo.' + @WODTable + ' t on (w.WOID = t.' + @WODTableWOIDField + ') 
			where   w.WOTypeID = ' + convert(varchar, @WOTypeID)
				+ ' and w.NextActionDate < ''' + @DateBefore + ''''
--				+ ' and w.NextActionSequence = 1000 '	-- 2006/08/10 - DP
				+ @WOArchiveTypeFilter
				+ ' set @ReplaceErrorOut = @@Error'
	execute sp_executesql @SQL, @ParamDefinition, @ReplaceErrorOut = @ErrorNr output
	if @ErrorNr <> 0 goto ErrorHandler

/* jkn 2006/04/28 Added new #Table to store Table Key IDs
	START*/
	--extract Table Key ID's 
	create table #KeyIDs (KeyID int)
	set @ParamDefinition = '@ReplaceErrorOut int OUTPUT'
	set @SQL = 	'insert into #KeyIDs select distinct t.' +   @WODTableKeyField + ' 
			from 	dbo.WO w inner join dbo.' + @WODTable + ' t on (w.WOID = t.' + @WODTableWOIDField + ') 
			where   w.WOTypeID = ' + convert(varchar, @WOTypeID)
				+ ' and w.NextActionDate < ''' + @DateBefore + ''''
--				+ ' and w.NextActionSequence = 1000 '	-- 2006/08/10 - DP
				+ @WOArchiveTypeFilter
				+ ' set @ReplaceErrorOut = @@Error'
	execute sp_executesql @SQL, @ParamDefinition, @ReplaceErrorOut = @ErrorNr output
	if @ErrorNr <> 0 goto ErrorHandler
/* jkn 2006/04/28 END*/


	select	@WOIDCount = count(*), @MinWOID = min(WOID), @MaxWOID = max(WOID) from #WOIDS

	if @WOIDCount > 0
	begin
		--count the source records
		set @ParamDefinition = '@ReplaceCount int OUTPUT, @ReplaceMinTableKey int OUTPUT, 
					@ReplaceMaxTableKey int OUTPUT, @ReplaceErrorOut int OUTPUT'
		set @SQL = 	'select @ReplaceCount =  COUNT(t.' + @WODTableKeyField + '),
					@ReplaceMinTableKey = min(t.' + @WODTableKeyField + '), 
					@ReplaceMaxTableKey = max(t.' + @WODTableKeyField + ')
				from 	dbo.' + @WODTable + ' t
				where   t.' + @WODTableWOIDField + ' in (select distinct WOID from #WOIDs)'
					+ @WOArchiveTypeFilter
					+ ' set @ReplaceErrorOut = @@Error'
		execute sp_executesql @SQL, @ParamDefinition, 	@ReplaceCount = @SrcRecordCount output,
								@ReplaceMinTableKey = @MinTableKey output,
								@ReplaceMaxTableKey = @MaxTableKey output,
								@ReplaceErrorOut = @ErrorNr output
		if @ErrorNr <> 0 goto ErrorHandler


		--determine the table fields
		exec prc_GetTableColumnList @WODTable, @TableFields output

		--create archive table if it does not exist
		set @SQL = 'if not exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[' + @ArchiveTargetTable + ']'') 
			    and OBJECTPROPERTY(id, N''IsUserTable'') = 1)  
			    begin 
				select * into dbo.' + @ArchiveTargetTable + ' from ' + @WODTable + ' where 1>2 
	
				ALTER TABLE [dbo].[' + @ArchiveTargetTable + '] WITH NOCHECK ADD 
					CONSTRAINT [' + @ArchiveTargetTable + '_PK] PRIMARY KEY  CLUSTERED 
					(
						[' + @WODTableKeyField + ']
					)  ON [PRIMARY] 
			    end'
		execute (@SQL)
	
	
		declare @TranName varchar(20)
		select @TranName = 'MoveData'
	
		BEGIN TRANSACTION @TranName
	
		--copy data from source table to target table
		set @ParamDefinition = '@ReplaceErrorOut int OUTPUT'
		set @SQL = 	'set identity_insert dbo.' + @ArchiveTargetTable + ' on 
				insert into dbo.' + @ArchiveTargetTable + ' (' + @TableFields + ') 
				select 	' + @TableFields + '
				from 	dbo.' + @WODTable + ' t 
				where 	t.' + @WODTableWOIDField + ' in (select distinct WOID from #WOIDs)'
					+ @WOArchiveTypeFilter
					+ ' set @ReplaceErrorOut = @@Error'
				+ ' set identity_insert dbo.' + @ArchiveTargetTable + ' off'
		execute sp_executesql @SQL, @ParamDefinition, @ReplaceErrorOut = @ErrorNr output
		if @ErrorNr <> 0 
		begin
			ROLLBACK TRANSACTION @TranName
			goto ErrorHandler
		end

		--delete the data from the source table
		set @ParamDefinition = '@ReplaceErrorOut int OUTPUT'
		set @SQL = 	'delete [' + @WODTable + '] 
				from	dbo.' + @WODTable + ' t
				where 	t.' + @WODTableWOIDField + ' in (select distinct WOID from #WOIDs)'
				+ @WOArchiveTypeFilter
				+ ' set @ReplaceErrorOut = @@Error'
		execute sp_executesql @SQL, @ParamDefinition, @ReplaceErrorOut = @ErrorNr output
		if @ErrorNr <> 0 
		begin
			ROLLBACK TRANSACTION @TranName
			goto ErrorHandler
		end

		--count the records transferred into target table
		set @ParamDefinition = '@ReplaceOUT int OUTPUT'
		set @SQL = 	'select @ReplaceOUT =  count(t.' + @WODTableKeyField + ') 
				from	dbo.' + @ArchiveTargetTable + ' t
				where 	t.' + @WODTableWOIDField + ' in (select distinct WOID from #WOIDs)  
					AND '+@WODTableKeyField+' IN (SELECT KeyID FROM #KeyIDs)' /*2006/04/28 - jkn - Added new select from #KeyIDs*/
					+ @WOArchiveTypeFilter
		execute sp_executesql @SQL, @ParamDefinition, @ReplaceOUT = @TrgInsertRowCount output
	
		--count the records deleted from original table
		set @ParamDefinition = '@ReplaceOUT int OUTPUT'
		set @SQL = 	'SELECT @ReplaceOUT =  COUNT(' + @WODTableKeyField + ') 
				from	dbo.' + @WODTable + ' t
				where 	t.' + @WODTableWOIDField + ' in (select distinct WOID from #WOIDs)'
					+ @WOArchiveTypeFilter
		execute sp_executesql @SQL, @ParamDefinition, @ReplaceOUT = @SrcDeleteRowCount output
	
		--confirm that move and delete was correct before removing records
		IF @SrcRecordCount = @TrgInsertRowCount AND @SrcDeleteRowCount=0
		BEGIN
			SET @Success = 1
	
			-- create 1 WOHistory entry per WOID for the type 3 archive
			if @WOArchiveTypeID = 3
			  begin
				set @SQL = 
					'insert into WOHistory (WOID, WOActionID, WOActionNote, WOActionDate, WOActionUser) '
					+ ' select distinct WOID, 9999, ''Records were archived'', ''' 
						+ convert(varchar,getdate(),113) + ''', ''' + @RunUser + ''' 
					from 	#WOIDS'
				execute (@SQL)
			  end
	
			COMMIT TRANSACTION @TranName
		END
		IF @SrcRecordCount <> @TrgInsertRowCount OR @SrcDeleteRowCount<>0
		BEGIN
			SET @Success= 0
			ROLLBACK TRANSACTION @TranName
		END

		--Return Success flag
		select 	@Success Success, @TrgInsertRowCount TransferCount, 
			isnull(@MinWOID,0) MinWOID, isnull(@MaxWOID,0) MaxWOID,
			isnull(@MinTableKey,0) MinTableKey, isnull(@MaxTableKey,0) MaxTableKey

	end
	else	begin
		--zero rows moved
		select 	1 Success, 0 TransferCount, 
			isnull(@MinWOID,0) MinWOID, isnull(@MaxWOID,0) MaxWOID,
			isnull(@MinTableKey,0) MinTableKey, isnull(@MaxTableKey,0) MaxTableKey
	end

	--cleanup
	drop table #WOIDS
/* jkn 2006/04/28 - Added Drop*/
	DROP TABLE #KeyIDs 

	return 0

ErrorHandler:
	drop table #WOIDS
/* jkn 2006/04/28 - Added Drop*/
	DROP TABLE #KeyIDs 

	declare @Message varchar(500)
	set @Message 	= 'Error ' + convert(varchar,@ErrorNr) 
			+ ' [prc_WOArchive_Move @WOArchiveSettingID = ' + convert(varchar,@WOArchiveSettingID)
			+ ' @RunDate = ' + convert(varchar(11), @RunDate, 106)
	raiserror (@Message,16,1)
	
set nocount off
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE  PROCEDURE [dbo].[prc_WOArchive_UpdateField] 
/**
**AUTHOR	: Johan Khn
**DATE CREATED	: 2005/07/12
**PURPOSE	: This storproc updates a field value in a source table. 
		  Can only update string fields at the moment.
		  This sp used to update/clear large text fields (like WODFileLog.FileContent) in order to free up disk space.
		: The field data is not moved/stored, but remove completely.
**NOTES		: 2005/12/13 AD Change sp - improve the mechanism of update and also add better error handling 
**			     this sp is now consistent with the Move sp
**
**/

	@WOArchiveSettingID int,
	@WOTypeID int, 
	@WODTable varchar(100), 
	@WODTableField varchar(100),
	@WODTableWOIDField varchar(10), 
	@WODTableKeyField varchar(100), 
	@WOIDArchiveAgeLimit int,
	@RunDate datetime
AS

BEGIN 
set nocount on

	declare @SQL nvarchar(2000), @ParamDefinition nvarchar(200), @SrcRecordCount int,
		@UpdateRowCount int, @Success int, @ErrorNr int,
		@WOIDCount int, @MinWOID int, @MaxWOID int, @MinTableKey int, @MaxTableKey int,
		@FieldContentNote varchar(50)

	set @Success = 0

	--extract the value to which the field must be updated
	select @FieldContentNote = isnull(FieldValue,'') from WOArchiveFilter where WOArchiveSettingID = @WOArchiveSettingID

	--extract WOID's
	declare @DateBefore varchar(11)
	select 	@DateBefore = convert(varchar(11), dateadd(day, -@WOIDArchiveAgeLimit, @RunDate))

	create table #WOIDS (WOID int)

	set @ParamDefinition = '@ReplaceErrorOut int OUTPUT'
	set @SQL = 	'insert into #WOIDs select distinct t.' +   @WODTableWOIDField + ' 
			from 	dbo.WO w inner join dbo.' + @WODTable + ' t on (w.WOID = t.' + @WODTableWOIDField + ') 
			where   w.WOTypeID = ' + convert(varchar, @WOTypeID)
				+ ' and w.NextActionDate < ''' + @DateBefore + ''''
				+ ' and w.NextActionSequence = 1000 
				    and t.[' + @WODTableField + '] not like ''' + @FieldContentNote + '%''
			set @ReplaceErrorOut = @@Error'
	execute sp_executesql @SQL, @ParamDefinition, @ReplaceErrorOut = @ErrorNr output
	if @ErrorNr <> 0 goto ErrorHandler

	select	@WOIDCount = count(*), @MinWOID = min(WOID), @MaxWOID = max(WOID) from #WOIDS

	if @WOIDCount > 0
	begin
		--count the source records
		set @ParamDefinition = '@ReplaceCount int OUTPUT, @ReplaceMinTableKey int OUTPUT, 
					@ReplaceMaxTableKey int OUTPUT, @ReplaceErrorOut int OUTPUT'
		set @SQL = 	'select @ReplaceCount =  COUNT(t.' + @WODTableKeyField + '),
					@ReplaceMinTableKey = min(t.' + @WODTableKeyField + '), 
					@ReplaceMaxTableKey = max(t.' + @WODTableKeyField + ')
				from 	dbo.' + @WODTable + ' t
				where   t.' + @WODTableWOIDField + ' in (select distinct WOID from #WOIDs) 
				set @ReplaceErrorOut = @@Error'
		execute sp_executesql @SQL, @ParamDefinition, 	@ReplaceCount = @SrcRecordCount output,
								@ReplaceMinTableKey = @MinTableKey output,
								@ReplaceMaxTableKey = @MaxTableKey output,
								@ReplaceErrorOut = @ErrorNr output
		if @ErrorNr <> 0 goto ErrorHandler

		declare @TranName varchar(20)
		select @TranName = 'UpdateData'
	
		BEGIN TRANSACTION @TranName

			--update the data in the given field of the source table
			set @ParamDefinition = '@ReplaceRowCountOut int OUTPUT'
			set @SQL = 	'update [dbo].[' + @WODTable + '] 
					set 	[' + @WODTableField + '] = ''' + @FieldContentNote + '''
					where	[' + @WODTableWOIDField + '] in (select distinct WOID from #WOIDs) 

					select	@ReplaceRowCountOut = @@RowCount'

			execute sp_executesql @SQL, @ParamDefinition, 	@ReplaceRowCountOut = @UpdateRowCount output

		if @ErrorNr = 0 and (@SrcRecordCount = @UpdateRowCount)
		begin
			set @Success = 1
			COMMIT TRANSACTION @TranName
		end
		else
		begin
			set @Success = 0
			ROLLBACK TRANSACTION @TranName
			goto ErrorHandler
		end		
	end
	else
	begin
		--no records to update
		set @Success = 1
		set @UpdateRowCount = 0
	end

	--return Success flag
	select 	@Success Success, @UpdateRowCount UpdateCount, 
		isnull(@MinWOID,0) MinWOID, isnull(@MaxWOID,0) MaxWOID,
		isnull(@MinTableKey,0) MinTableKey, isnull(@MaxTableKey,0) MaxTableKey

	drop table #WOIDS
	return 0

ErrorHandler:
	drop table #WOIDS

	declare @Message varchar(500)
	set @Message 	= 'Error ' + convert(varchar,@ErrorNr) 
			+ ' [prc_WOArchive_UpdateField @WOArchiveSettingID = ' + convert(varchar,@WOArchiveSettingID)
			+ ' @RunDate = ' + convert(varchar(11), @RunDate, 106)
	raiserror (@Message,16,1)

set nocount off
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE  PROCEDURE [dbo].[prc_WOArchive_UpdateField1] 
/**
**AUTHOR	: Johan Khn
**DATE CREATED	: 2006/08/05
**PURPOSE	: This storproc updates a field value in a source table. This sp used to update/clear large text fields (like WODFileLog.FileContent) in order to free up disk space.
		: The field data is not moved/stored, but remove completely.
**NOTES		:
**		: 2006/08/05 - jkn - copy of the original sp of 2005/07/12 created as a new sp.
**/
@WOTypeID int,
@WOArchiveSettingID int,
@WODTable nvarchar(200),
@WODTableField varchar(200), --Has to be a varchar(20+) or text field
@WODTableWOIDField varchar(200),
--@ArchiveTargetTable varchar(200),
@WODTableKeyField varchar(200),
@WOIDStart int,
@WOIDEnd int,
@WODTableKeyStart int,
@WODTableKeyEnd int,
@RunUser varchar(100),
@RunDate datetime,
@1stRun int, -- 1st run when =0, later runs when >0,
@FieldContentNote varchar(30) -- This is the value that willl be used to update the req. field

AS
SET NOCOUNT ON
DECLARE @SQL nvarchar(2000), @ParamDefinition nvarchar(200), @SrcRecordCount int,
	@SrcDeleteRowCount int, @Success int
SET @Success = 0
	--Count the Src records
	SET @ParamDefinition = '@ReplaceOUT int OUTPUT'
	SET @SQL = 'SELECT @ReplaceOUT =   COUNT(dbo.'+@WODTable+'.'+@WODTableKeyField+')  
	FROM  dbo.'+@WODTable+' INNER JOIN WO ON WO.WOID =  dbo.'+@WODTable+'.'+@WODTableWOIDField+' 
	WHERE WO.WOTypeID = '+CONVERT(nvarchar,@WOTypeID)+' 
		AND '+@WODTableKeyField+'>'+CONVERT(nvarchar,@WODTableKeyStart)+' 
		AND '+@WODTableKeyField+'<='+CONVERT(nvarchar,@WODTableKeyEnd) 
	EXECUTE sp_executesql @SQL, @ParamDefinition, @ReplaceOUT = @SrcRecordCount OUTPUT
	--Determine the specific ID in Src table to Update according to WOType
	CREATE TABLE  #TableIDs (IDs int)
	SET @ParamDefinition = '@WODTable1 nvarchar(200) '
	SET @SQL = 'INSERT INTO #TableIDs SELECT  dbo.'+@WODTable+'.'+@WODTableKeyField+'  
	FROM  dbo.'+@WODTable+' INNER JOIN WO ON WO.WOID =  dbo.'+@WODTable+'.'+@WODTableWOIDField+' 
	WHERE WO.WOTypeID = '+CONVERT(nvarchar,@WOTypeID)+' 
		AND ' +@WODTableKeyField+'>'+CONVERT(nvarchar,@WODTableKeyStart)+' 
		AND '+@WODTableKeyField+'<='+CONVERT(nvarchar,@WODTableKeyEnd) 
	EXECUTE sp_executesql @SQL, @ParamDefinition, @WODTable1 = @WODTable
	--SELECT @SQL
	
BEGIN
	DECLARE @TranName VARCHAR(20)
	SELECT @TranName = 'RemoveField'

	BEGIN TRANSACTION @TranName

	--Update Src Table field
	DECLARE IDs CURSOR FOR
	SELECT IDs FROM #TableIDs
	OPEN IDs
	DECLARE @ID int
	FETCH NEXT FROM IDs INTO @ID
	WHILE (@@FETCH_STATUS = 0)
		BEGIN
		--Update Src Table Field HERE
		SET @SQL = 'UPDATE dbo.'+@WODTable+' 
			SET '+@WODTableField+'= '''+CONVERT(nvarchar,@FieldContentNote)+'''
			WHERE     '+@WODTableKeyField+'='+CONVERT(nvarchar,@ID)
		EXECUTE sp_executesql @SQL
		
   		FETCH NEXT FROM IDs INTO @ID
		END
	--SELECT @@CURSOR_ROWS
	CLOSE IDs
	DEALLOCATE IDs
	DROP TABLE #TableIDs

	--Count the Updated records in original table
	SET @ParamDefinition = '@ReplaceOUT int OUTPUT'
	SET @SQL = 'SELECT  @ReplaceOUT =  COUNT(dbo.'+@WODTable+'.'+@WODTableKeyField+')  
			FROM  dbo.'+@WODTable+' INNER JOIN WO ON WO.WOID =  dbo.'+@WODTable+'.'+@WODTableWOIDField+' 
			WHERE WO.WOTypeID = '+CONVERT(varchar,@WOTypeID)+' 
				AND '+@WODTableKeyField+'>'+CONVERT(nvarchar,@WODTableKeyStart)+' 
				AND '+@WODTableKeyField+'<='+CONVERT(nvarchar,@WODTableKeyEnd)+'
				AND '+@WODTableField+' LIKE ''%'+CONVERT(nvarchar,@FieldContentNote)+'%'''
	EXECUTE sp_executesql @SQL, @ParamDefinition, @ReplaceOUT = @SrcDeleteRowCount OUTPUT

	--Confirm that maove and delete was correct before removing records
	IF @SrcRecordCount = @SrcDeleteRowCount
	BEGIN
	SET @Success = 1
	COMMIT TRANSACTION @TranName
	END
	IF @SrcRecordCount <> @SrcDeleteRowCount
	BEGIN
	SET @Success= 0
	ROLLBACK TRANSACTION @TranName
	END
	

END



--Return Success flag
SELECT @Success as Success,  @SrcDeleteRowCount AS UpdateCount

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



create procedure [dbo].[prc_WOCreate]
	@WOType varchar(50),
	@User varchar(50)
/*
	1. Create a new work object with the given type in the WO table
	2. Log the WOID creation in WOHistory
*/
as
	declare @WOTypeID int, @WOID int, @WOAction varchar(50)
begin
	set @WOAction = 'Create WO'

	select @WOTypeID = WOTypeID from WOType where [Name] = @WOType

	insert into WO (WOTypeID, WOStatusID, WOPriorityID, NextActionSequence, NextActionDate, WOSource)
	values (@WOTypeID, 1, 1, [dbo].[GetActionSequenceNext](@WOType, @WOAction), getdate(), 1)

	set	@WOID = @@identity

	insert into WOHistory (WOID, WOActionID, WOActionNote, WOActionDate, WOActionUser)
	select 	@WOID, WOActionID, [Description], getdate(), @User 
	from 	WOAction 
	where 	[Name] = @WOAction

	select @WOID WOID
end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE [dbo].[prc_WODFaxLog_insert]

/**
**AUTHOR		: JK�hn
**DATE CREATED	: 2003/07/30
**PURPOSE		: This storproc inserts a new record in the WODFaxLog  table.
**NOTES		:
**
**/
	(@WOID_1 		[int],
	 @FContactName_2 	varchar(250),
	 @FContactFax_3 	varchar(100),
	 @FBody_4 		varchar(250),
	 @FSubject_5 		varchar(100) )

AS INSERT INTO [WODFaxLog] 
	 ( [WOID],
	 [FContactName],
	 [FContactFax],
	 [FBody],
	 [FSubject]) 
 
VALUES 
	( @WOID_1,
	 @FContactName_2,
	 @FContactFax_3,
	 @FBody_4,
	 @FSubject_5)


SELECT @@IDENTITY AS WODFileLogID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO










CREATE PROCEDURE [dbo].[prc_WODFaxLog_select]
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2003/08/16
**PURPOSE		: This storproc selects Fax doc detail 
**NOTES		: 
**			: 
**
**/
	(@SubmitDate			varchar(50),
	@MancoName			varchar(100))

AS 
SET NOCOUNT ON

SELECT     
	WFaxL.WOID AS WOID, 
	WA.Name AS [Action], 
	dbo.fMakeUKDateTime(WH.WOActionDate)  AS ActionDate, 
	WFaxL.FSubject AS FundManager, 
	WFileL.BackUpFileName AS [FileName], 
             WFileL.BackupFileLocation + '\' + WFileL.BackUpFileName AS FilePath
FROM         dbo.WODFaxLog WFaxL INNER JOIN
                      dbo.WO WO ON WFaxL.WOID = WO.WOID INNER JOIN
                      dbo.WODFileLog WFileL ON WO.WOID = WFileL.WOID INNER JOIN
                      dbo.WOHistory WH ON WO.WOID = WH.WOID INNER JOIN
                      dbo.WOAction WA ON WH.WOActionID = WA.WOActionID
WHERE     (WH.WOActionID = 98) 
	AND (WO.WOTypeID = 22)
	AND DATEDIFF(d,@SubmitDate, WH.WOActionDate )  = 0 
	AND WFaxL.FSubject LIKE '%'+@MancoName+'%'
	AND WFileL.CFFileSettingID IN (84, 85)









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE [dbo].[prc_WODFileLog_insert]
/**
**AUTHOR		: JK�hn
**DATE CREATED	: 2003/05/08
**PURPOSE		: This storproc inserts a new record in the WODFileLog  table.
**NOTES		:
			: 2004/12/28 - changed jkn - added the FileContentKey field to the inserted fields
**
**/
	(@WOID_1 			[int],
	 @CFFileSettingID_2 		[int],
	 @FileName_3 			[varchar](200),
	 @FileLocation_4 		[varchar](200),
	 @FileSize_5 			[varchar](100),
	 @FileContent_6 		[text],
	 @BackUpFileName_7 		[varchar](200),
	 @BackupFileLocation_8 	[varchar](200),
	 @FileContentKey		[varchar](200) )

AS 
INSERT INTO [dbo].[WODFileLog] 
	 ( [WOID],
	 [CFFileSettingID],
	 [FileName],
	 [FileLocation],
	 [FileSize],
	 [FileContent],
	 [BackUpFileName],
	 [BackupFileLocation],
	 [FileContentKey] ) 
 
VALUES 
	( @WOID_1,
	 @CFFileSettingID_2,
	 @FileName_3,
	 @FileLocation_4,
	 CASE WHEN ISNUMERIC (@FileSize_5) =0 THEN 0 ELSE @FileSize_5 END,
	 @FileContent_6,
	 @BackUpFileName_7,
	 @BackupFileLocation_8,
	 @FileContentKey)

SELECT @@IDENTITY AS WODFileLogID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO










CREATE PROCEDURE [dbo].[prc_WODFileLog_update]
/**
**AUTHOR		: J K�hn
**DATE CREATED	: 2004/10/06
**PURPOSE		: This storproc updates the FileContentKey  in table WODFileLog.
**NOTES		:
**
**/
	(@WOID_1 			[int],
	 @WODFileLogID 		[int],
	 @FileContentKey		[varchar](200) )

AS 

UPDATE WODFileLog
SET FileContentKey = @FileContentKey
WHERE 	WOID = @WOID_1 
		AND WODFileLogID = @WODFileLogID

SELECT @@IDENTITY AS WODFileLogID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO










CREATE PROCEDURE [dbo].[prc_WODFileLog_update1]
/**
**AUTHOR		: J K�hn
**DATE CREATED	: 2004/10/19
**PURPOSE		: This storproc updates the CFFileSettingID  in table WODFileLog.
**NOTES		:
**
**/
	(@WOID_1 			[int],
	 @WODFileLogID 		[int],
	 @CFFileSettingID_new		[int])

AS 

UPDATE WODFileLog
SET CFFileSettingID = @CFFileSettingID_new
WHERE 	WOID = @WOID_1 
		AND WODFileLogID = @WODFileLogID

SELECT @@IDENTITY AS WODFileLogID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO











CREATE  PROCEDURE dbo.[prc_WODFileLog_update2]
/**
**AUTHOR		: Vilen Muruvan
**DATE CREATED	: 2005/02/14
**PURPOSE		: This storproc updates the BackUpFileName
**NOTES		:
**/
	(@WODFileLogID_1 	[int],
	 @BackUpFileName_2 	[varchar](200))

AS UPDATE [WODFileLog] 

SET  [BackUpFileName]	 = @BackUpFileName_2 

WHERE 
	( [WODFileLogID]	 = @WODFileLogID_1)










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO










CREATE PROCEDURE [dbo].[prc_WODMailAttachmentLog_insert]
/**
**AUTHOR		: JK�hn
**DATE CREATED	: 2004/10/19
**PURPOSE		: This storproc insert a relation into table WODMaillAttachmentLog for WODMailLogID and WODFileLogID
**NOTES		:
**
**/
	(@WOID_1 		[int],
	 @WODMailLogID_2 	[int],
	 @WODFileLogID_3 	[int])

AS INSERT INTO [WODMailAttachmentLog] 
	 ( [WOID],
	 [WODMailLogID],
	 [WODFileLogID]) 
 
VALUES 
	( @WOID_1,
	 @WODMailLogID_2,
	 @WODFileLogID_3)

SELECT @@IDENTITY AS WODMailAttachmentLogID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO











CREATE  PROCEDURE dbo.prc_WODMailLog_Classify
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2004/10/19
**PURPOSE		: This stored proc compares the input parameters with values stored in CFMailAttachmentClassify in order to return a CFFileSettingID
**NOTES		: 
**			: 
**
**/

	@WOID int,
	@CFMailSettingID 	int,
	@MDirection		varchar(10),
	@MSender		varchar(250),
	@MReceiver		varchar(250),
	@MSubject		varchar(1000),
	@MBody		varchar(1000),
	@WODFileLogID 	int,
	@CFFileSettingID 	int,
	@FileName 		varchar(1000)
	


AS
SET NOCOUNT ON

DECLARE @CFFileSettingID1	 int
SET @CFFileSettingID1 = @CFFileSettingID

DECLARE 	@SuccessID	int -- Values returned - 1 for success and 0 for error
SELECT	@SuccessID = 0

--First check for Insert or Update
if exists		(SELECT * FROM CFMailAttachmentClassify CFAC (NOLOCK)
		WHERE 	@MSender LIKE '%'+CFAC.MSender+'%'
				--AND @MSender LIKE '%'+CFAC.MSender+'%'
				AND @MReceiver LIKE '%'+CFAC.MReceiver+'%'
				AND @MSubject LIKE '%'+CFAC.MSubject+'%'
				AND @MBody LIKE '%'+CFAC.MBody+'%'
				AND @FileName LIKE '%'+CFAC.[FileName] +'%'
		)
  begin
	begin transaction
	
		SET @CFFileSettingID1 = (SELECT MAX(CFAC.CFFileSettingID)  
					FROM CFMailAttachmentClassify CFAC (NOLOCK)
					WHERE 	@MSender LIKE '%'+CFAC.MSender+'%'
						--AND @MSender LIKE '%'+CFAC.MSender+'%'
						AND @MReceiver LIKE '%'+CFAC.MReceiver+'%'
						AND @MSubject LIKE '%'+CFAC.MSubject+'%'
						AND @MBody LIKE '%'+CFAC.MBody+'%'
						AND @FileName LIKE '%'+CFAC.[FileName] +'%' )

	if @@ERROR <> 0 
	  begin
		rollback transaction
		SELECT	@SuccessID = 0
	  end
	else
	  begin
		commit transaction
		SELECT	@SuccessID = 1
	  end

  end


/*
SELECT     CFMailAttachmentClassifyID, MSender, MReceiver, MSubject, MBody, FileName, CFFileSettingID
FROM         dbo.CFMailAttachmentClassify


SELECT     dbo.WO.WOID, dbo.WO.WOTypeID, dbo.WO.WOStatusID, dbo.WO.NextActionSequence, dbo.WO.NextActionDate, WML.CFMailSettingID, 
                      WML.MDirection, WML.MSender, WML.MReceiver, WML.MSubject, WML.MBody, WFL.WODFileLogID, WFL.CFFileSettingID, WFL.FileName, 
                      WFL.FileLocation, WFL.BackUpFileName, WFL.BackupFileLocation
FROM         dbo.WO INNER JOIN
                      dbo.WODMailLog WML ON dbo.WO.WOID = WML.WOID INNER JOIN
                      dbo.WODMailAttachmentLog WMAL ON dbo.WO.WOID = WMAL.WOID AND WML.WODMailLogID = WMAL.WODMailLogID INNER JOIN
                      dbo.WODFileLog WFL ON dbo.WO.WOID = WFL.WOID AND WMAL.WODFileLogID = WFL.WODFileLogID
WHERE     (dbo.WO.NextActionSequence = 30) AND (dbo.WO.WOStatusID = 2)
*/

SELECT @SuccessID AS SuccessID, @CFFileSettingID1 AS CFFileSettingID_New










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE [dbo].[prc_WODMailLog_Insert]
/**
**AUTHOR		: JK�hn
**DATE CREATED	: 2003/05/29
**PURPOSE		: This storproc inserts a new record in the WODMailLog  table.
**NOTES		:
**
**/
	(@CFMailSettingID_1 		[int],
	 @WOID_2 			[int],
	 @MDirection_3 		[varchar](10),
	 @MSender_4 			[varchar](100),
	 @MReceiver_5 		[varchar](100),
	 @MCC_6 			[varchar](100),
	 @MBCC_7 			[varchar](100),
	 @MSubject_8 			[varchar](1000),
	 @MBody_9 			[varchar](1000),
	 @MAttachmentProcessed_10 	[char](1))

AS INSERT INTO [dbo].[WODMailLog] 
	 ( [CFMailSettingID],
	 [WOID],
	 [MDirection],
	 [MSender],
	 [MReceiver],
	 [MCC],
	 [MBCC],
	 [MSubject],
	 [MBody],
	 [MAttachmentProcessed]) 
 
VALUES 
	( @CFMailSettingID_1,
	 @WOID_2,
	 @MDirection_3,
	 @MSender_4,
	 @MReceiver_5,
	 @MCC_6,
	 @MBCC_7,
	 @MSubject_8,
	 @MBody_9,
	 @MAttachmentProcessed_10)

SELECT @@IDENTITY AS WODMailLogID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE [dbo].[prc_WODetail_insert]
/**
**AUTHOR		: JKhn
**DATE CREATED	: 2003/05/08
**PURPOSE		: This storproc inserts a new record in the WODetail  table.
**NOTES		:
**
**/

	(@WODetailTypeID_1 	[int],
	 @WOTypeID_2 	[int],
	 @WOID_3 		[int],
	 @DetailID_4 		[int])

AS INSERT INTO [dbo].[WODetail] 
	 ([WODetailTypeID],
	 [WOTypeID],
	 [WOID],
	 [DetailID]) 
 
VALUES 
	( @WODetailTypeID_1,
	 @WOTypeID_2,
	 @WOID_3,
	 @DetailID_4)


SELECT @@IDENTITY AS WODetailID_Returned

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO









CREATE  PROCEDURE [dbo].[prc_WOHistory_insert]
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2003/05/09
**PURPOSE		: This storproc inserts a new record in the WOHistory table. 
**NOTES		:
**
**/
	(@WOID_1 		[int],
	 @WOActionID_2 	[int],
	 @WOActionNote_3 	[varchar](300),
	 @WOActionDate_4 	[varchar] (50),
	 @WOActionUser_5 	[varchar](50))

AS INSERT INTO [dbo].[WOHistory] 
	 ( [WOID],
	 [WOActionID],
	 [WOActionNote],
	 [WOActionDate],
	 [WOActionUser]) 
 
VALUES 
	( @WOID_1,
	 @WOActionID_2,
	 @WOActionNote_3,
	 CASE WHEN ISDATE (@WOActionDate_4) = 0 THEN CURRENT_TIMESTAMP ELSE  @WOActionDate_4 END,
	 @WOActionUser_5)

SELECT @@IDENTITY AS WOHistoryID_Returned








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE   PROCEDURE [dbo].[prc_WOHistory_insert1]
/**
**AUTHOR		: Tessa Marais
**DATE CREATED	: 2003/05/09
**PURPOSE		: This storproc inserts a new record in the WOHistory table. 
**NOTES		:	Return identity 
**
**/
	(@WOID_1 		[int],
	 @WOActionID_2 	[int],
	 @WOActionNote_3 	[varchar](200),
	 @WOActionDate_4 	[varchar] (50),
	 @WOActionUser_5 	[varchar](50))

AS INSERT INTO [dbo].[WOHistory] 
	 ( [WOID],
	 [WOActionID],
	 [WOActionNote],
	 [WOActionDate],
	 [WOActionUser]) 
 
VALUES 
	( @WOID_1,
	 @WOActionID_2,
	 @WOActionNote_3,
	 CASE WHEN ISDATE (@WOActionDate_4) = 0 THEN CURRENT_TIMESTAMP ELSE  @WOActionDate_4 END,
	 @WOActionUser_5)
return @@IDENTITY









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE [dbo].[prc_WORelationship_insert]
/**
**AUTHOR		: Johan K�hn
**DATE CREATED	: 2003/05/13
**PURPOSE		: This storproc inserts a new record in the WORelationship table. 
**NOTES		:
**
**/

	(@WOIDLeft_1 	[int],
	 @WOIDRight_2 	[int],
	 @WORelationshipTypeID_3 	[int])

AS INSERT INTO [dbo].[WORelationship] 
	 ( [WOIDLeft],
	 [WOIDRight],
	 [WORelationshipTypeID]) 
 
VALUES 
	( @WOIDLeft_1,
	 @WOIDRight_2,
	 @WORelationshipTypeID_3)


SELECT @@IDENTITY AS WORelationshipID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE  PROCEDURE [dbo].[prc_WORelationship_insert_1]
/**
**AUTHOR		: Christiaan Pretorius
**DATE CREATED		: 2005/11/17
**PURPOSE		: This storproc inserts a new record in the WORelationship table. 
**NOTES			: It does not return an ID to be used, so can use it in Stored Procedures without affecting Linx
**
**/

	(@WOIDLeft_1 	[int],
	 @WOIDRight_2 	[int],
	 @WORelationshipTypeID_3 	[int])

AS INSERT INTO [dbo].[WORelationship] 
	 ( [WOIDLeft],
	 [WOIDRight],
	 [WORelationshipTypeID]) 
 
VALUES 
	( @WOIDLeft_1,
	 @WOIDRight_2,
	 @WORelationshipTypeID_3)


return @@IDENTITY









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.prc_WOTypeDetail_Delete
	@WODetailTypeID int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/
BEGIN 
DELETE FROM dbo.WOTypeDetail
WHERE
	WODetailTypeID = @WODetailTypeID


IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Delete Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Delete Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.prc_WOTypeDetail_Update

	@WODetailTypeID int = null ,
	@Name varchar(50) = null, 
	@Description varchar(100) = null, 
	@WOLocationTable varchar(100) = null, 
	@WOTypeID int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/
BEGIN
UPDATE dbo.WOTypeDetail 
SET	
	Name = CASE WHEN @Name IS NOT NULL THEN @Name ELSE Name END,
	Description = CASE WHEN @Description IS NOT NULL THEN @Description ELSE Description END,
	WOLocationTable = CASE WHEN @WOLocationTable IS NOT NULL THEN @WOLocationTable ELSE WOLocationTable END,
	WOTypeID = CASE WHEN @WOTypeID IS NOT NULL THEN @WOTypeID ELSE WOTypeID END
WHERE
	WODetailTypeID = @WODetailTypeID


SET @WODetailTypeID = ISNULL(SCOPE_IDENTITY(), -1)

IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Update Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Update Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


CREATE PROCEDURE dbo.prc_WOType_Delete
	@WOTypeID int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/
DELETE FROM dbo.WOType
WHERE
	WOTypeID = @WOTypeID


IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Delete Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Delete Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



CREATE PROCEDURE dbo.prc_WOType_Update

	@WOTypeID int = null ,
	@Name varchar(50) = null, 
	@Description varchar(100) = null, 
	@WOPriorityIDDefault int = null, 
	@WOStatusIDDefault int = null 	,	
	@Successful bit OUTPUT, --This is for displaying a message in Stadium
	@MessageOut varchar(150) OUTPUT --This is for displaying a message in Stadium

AS
/*
Author				: Created as part of the automated build by CodeSmith
DateCreated		: 24 November 2005
Notes					: 
*/

UPDATE dbo.WOType 
SET	
	Name = CASE WHEN @Name IS NOT NULL THEN @Name ELSE Name END,
	Description = CASE WHEN @Description IS NOT NULL THEN @Description ELSE Description END,
	WOPriorityIDDefault = CASE WHEN @WOPriorityIDDefault IS NOT NULL THEN @WOPriorityIDDefault ELSE WOPriorityIDDefault END,
	WOStatusIDDefault = CASE WHEN @WOStatusIDDefault IS NOT NULL THEN @WOStatusIDDefault ELSE WOStatusIDDefault END
WHERE
	WOTypeID = @WOTypeID


SET @WOTypeID = ISNULL(SCOPE_IDENTITY(), -1)

IF (@@ERROR = 0)
BEGIN
	SET @Successful = 1
	SET @MessageOut = 'Update Executed Successfully'
END
ELSE
BEGIN
	SET @Successful = 0
	SET @MessageOut = 'Update Failed'
END
RETURN ISNULL(@@ROWCOUNT, -1)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO










CREATE PROCEDURE [dbo].[prc_WO_Insert] 
/**
**AUTHOR		:
**DATE CREATED	:
**PURPOSE		: This storproc inserts a new record in the WO table. The WOTypeID & WOStatusID values are passed and the new WOID returned.
**NOTES		:
**
select WOID, WOTypeID, WOStatusID from dbo.WO
**/

	(@WOTypeID_1 		[int],
	 @WOStatusID_2 		[int],
	 @WOPriorityID_3 		[int],
	 @NextActionSequence_4 	[int],
	 @NextActionDate_5 		[varchar],
	 @WOSource_6 		[int])

AS INSERT INTO [dbo].[WO] 
	 ( [WOTypeID],
	 [WOStatusID],
	 [WOPriorityID],
	 [NextActionSequence],
	 [NextActionDate],
	 [WOSource]) 
 
VALUES 
	( @WOTypeID_1,
	 @WOStatusID_2,
	 @WOPriorityID_3,
	 @NextActionSequence_4,
	 CASE WHEN ISDATE(@NextActionDate_5) = 0 THEN CURRENT_TIMESTAMP ELSE @NextActionDate_5 END,
	 @WOSource_6)

SELECT @@IDENTITY AS WOID_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE   PROCEDURE [dbo].[prc_WO_Insert_1] 
/**
**AUTHOR		: Tessa Marais
**DATE CREATED	:
**PURPOSE		: This storproc inserts a new record in the WO table. The WOTypeID & WOStatusID values are passed and the new WOID returned.
**NOTES		:	return value needed for stadium
**
select WOID, WOTypeID, WOStatusID from dbo.WO
**/

	(@WOTypeID_1 		[int],
	 @WOStatusID_2 		[int],
	 @WOPriorityID_3 		[int],
	 @NextActionSequence_4 	[int],
	 @NextActionDate_5 		[varchar],
	 @WOSource_6 		[int])

AS INSERT INTO [dbo].[WO] 
	 ( [WOTypeID],
	 [WOStatusID],
	 [WOPriorityID],
	 [NextActionSequence],
	 [NextActionDate],
	 [WOSource]) 
 
VALUES 
	( @WOTypeID_1,
	 @WOStatusID_2,
	 @WOPriorityID_3,
	 @NextActionSequence_4,
	 CASE WHEN ISDATE(@NextActionDate_5) = 0 THEN CURRENT_TIMESTAMP ELSE @NextActionDate_5 END,
	 @WOSource_6)


return @@IDENTITY









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE [dbo].[prc_WO_WOHistory_update1]
/**
**AUTHOR		: JK�hn
**DATE CREATED	: 2003/07/24
**PURPOSE		: This storproc updates the WO table. The prc is used when updating WOType 6 WO's which have bulked records in table WODTrgOrderMancoFS
**NOTES		:
**
**/
--Values passed
	(@WOID_1 			[int],
	 @BulkReq_2 			[varchar] (10),
	 @BulkID_3 			[int],
	 @NextActionSequence_4 	[int],
	 @NextActionDate_5 		[varchar] (20),
	 @WOActionID_6		[int],
	 @WOActionNote_7		[varchar] (200),
	 @WOActionUser_8		[varchar] (50),
	 @WOID_New_9 		[int])

AS 

/**Bulking **/
IF @BulkReq_2 = 'Y'
BEGIN
CREATE TABLE #WOIDList (WOID int)
INSERT INTO #WOIDList
	SELECT WOID 
	FROM WODTrgOrderMancoFS 
	WHERE 
		BulkReq = @BulkReq_2 AND
		BulkID = @BulkID_3

UPDATE WO SET 
	NextActionSequence = @NextActionSequence_4,
	NextActionDate = (CASE WHEN ISDATE(@NextActionDate_5)=0 THEN CURRENT_TIMESTAMP ELSE @NextActionDate_5 END)
	WHERE WOID IN (SELECT WOID FROM #WOIDList)

INSERT INTO [WOHistory]   ( [WOID],[WOActionID],[WOActionNote],[WOActionDate],[WOActionUser]) 
	SELECT 	WOID, 
		@WOActionID_6, 
		@WOActionNote_7, 
		(CASE WHEN ISDATE(@NextActionDate_5)=0 THEN CURRENT_TIMESTAMP ELSE @NextActionDate_5 END),
		@WOActionUser_8
	FROM #WOIDList

INSERT INTO [WORelationship]  	 ( [WOIDLeft],  [WOIDRight],  [WORelationshipTypeID]) 
	SELECT 	WOID, @WOID_New_9, 1 
	FROM #WOIDList

DROP TABLE #WOIDList

END

/**No Bulking **/
IF @BulkReq_2 = 'N'
BEGIN

UPDATE WO SET 
	NextActionSequence = @NextActionSequence_4,
	NextActionDate = (CASE WHEN ISDATE(@NextActionDate_5)=0 THEN CURRENT_TIMESTAMP ELSE @NextActionDate_5 END)
	WHERE WOID = @WOID_1

INSERT INTO [WOHistory]   ( [WOID],[WOActionID],[WOActionNote],[WOActionDate],[WOActionUser]) 
	VALUES 
	(@WOID_1,
	 @WOActionID_6,
	 @WOActionNote_7,
	 CASE WHEN ISDATE (@NextActionDate_5) = 0 THEN CURRENT_TIMESTAMP ELSE  @NextActionDate_5 END,
	 @WOActionUser_8)

INSERT INTO [WORelationship]  	 ( [WOIDLeft],  [WOIDRight],  [WORelationshipTypeID]) 
 	VALUES 
	( @WOID_1,
	 @WOID_New_9,
	 1)

END





SELECT @@ERROR AS ERROR_Returned
SELECT @@ROWCOUNT AS Rowcount_Returned









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO










CREATE PROCEDURE [dbo].[prc_WO_update]
/**
**AUTHOR		: JK�hn
**DATE CREATED	: 2003/05/08
**PURPOSE		: This storproc updates the WO table. The parameters are Update Flag & UpdateValue. Update Flag is a int value of 1= 'Yes' or 0 = 'No'
**NOTES		:
**
**/
--Flags
	( --@WOID_2_Flag			[int],
	 @WOTypeID_3_Flag 			[int],
	 @WOStatusID_4_Flag 			[int],
	 @WOPriorityID_5_Flag 			[int],
	 @NextActionSequence_6_Flag 		[int],
	 @NextActionSequence_6_DefaultFlag 	[int], -- Should the field be updated with the next default action ID from ActionPath table.
	 @NextActionDate_7_Flag 		[int],
	 @WOSource_8_Flag 			[int],
--Values passed
	 @WOID_1 			[int],
	 --@WOID_2 			[int],
	 @WOTypeID_3 		[int],
	 @WOStatusID_4 		[int],
	 @WOPriorityID_5 		[int],
	 @NextActionSequence_6 	[int],
	 @NextActionDate_7 		[varchar] (20),
	 @WOSource_8 		[int] )

AS 
--Build an update string 
DECLARE 	 @UpdateString			[varchar] (200)
DECLARE 	 @UpdateString_Exec		[varchar] (200)

SET @UpdateString = ' '

IF @WOTypeID_3_Flag = 1 	
		SET @UpdateString = @UpdateString + ',  [WOTypeID] = '+ CAST ( ' '+@WOTypeID_3+' ' AS varchar(200))

IF @WOStatusID_4_Flag = 1 	
		SET @UpdateString = @UpdateString + ',  [WOStatusID] = '+ CAST ( ' '+@WOStatusID_4+' ' AS varchar(200))

IF @WOPriorityID_5_Flag = 1 	
		SET @UpdateString = @UpdateString + ',  [WOPriorityID] = '+ CAST ( ' '+@WOPriorityID_5+' ' AS varchar(200))

IF @NextActionSequence_6_Flag = 1 AND @NextActionSequence_6_Flag = 0
		SET @UpdateString = @UpdateString + ',  [NextActionSequence] = '+ CAST ( ' '+@NextActionSequence_6+' ' AS varchar(200))


-- If selected the WO.NextActionSequence is updated with the next default action sequence as per the WOActionPath for that WOType table.
IF 	( @NextActionSequence_6_DefaultFlag = 1 AND @NextActionSequence_6_Flag = 0 ) 	OR
	( @NextActionSequence_6_DefaultFlag = 1 AND @NextActionSequence_6_Flag = 1 )	
	--Select The next required action sequence from the actionpath for this WOType. If no next action is found thne the next action sequence is set to 1000
	SET @NextActionSequence_6 = ( SELECT 
					CASE 	WHEN 	( SELECT MIN (ActionSequence) FROM WOActionPath 
								WHERE 	WOTypeID = ( SELECT  DISTINCT  dbo.WO.WOTypeID FROM dbo.WO WHERE dbo.WO.WOID = @WOID_1 )
										AND ActionRequired = 'Y'
										AND ActionSequence > ( SELECT  DISTINCT  dbo.WO.NextActionSequence FROM dbo.WO WHERE dbo.WO.WOID = @WOID_1 )
							) IS NULL
						THEN 1000
						ELSE	( SELECT MIN (ActionSequence) FROM WOActionPath
								WHERE 	WOTypeID = ( SELECT  DISTINCT  dbo.WO.WOTypeID FROM dbo.WO WHERE dbo.WO.WOID = @WOID_1 )
										AND ActionRequired = 'Y' 
										AND ActionSequence > ( SELECT  DISTINCT  dbo.WO.NextActionSequence FROM dbo.WO WHERE dbo.WO.WOID = @WOID_1 )
							)
						END )

		SET @UpdateString = @UpdateString + ', [NextActionSequence] = '+ CAST ( ' '+@NextActionSequence_6+' ' AS varchar(200))

IF @NextActionDate_7_Flag = 1 	AND ISDATE (@NextActionDate_7) = 1
		SET @UpdateString = @UpdateString + ',  [NextActionDate] = '+ CAST ( ' '+@NextActionDate_7+' ' AS varchar(200))

IF @NextActionDate_7_Flag = 1 	AND ISDATE (@NextActionDate_7) = 0
		SET @UpdateString = @UpdateString + ',  [NextActionDate] = '+ CAST ( ' CURRENT_TIMESTAMP ' AS varchar(200))

IF @WOSource_8_Flag = 1 	
		SET @UpdateString = @UpdateString + ',  [WOSource] = '+ CAST ( ' '+@WOSource_8+' ' AS varchar(200))

/**Test execute string
SELECT CHAR(39) + ( ('UPDATE [fbc_idb_dev].[dbo].[WO] SET ') + @UpdateString + (' WHERE  [WOID] = ' ) + (CONVERT (varchar (200),  ' '+@WOID_1+' ')) ) + CHAR(39) AS FirstString
**/
SET @UpdateString = ( SELECT SUBSTRING (@UpdateString, 3, LEN (@UpdateString) ) )
--Test - SELECT @UpdateString as Test1
SET @UpdateString_Exec =  ( CHAR(39) + ( ('UPDATE [dbo].[WO] SET ') + @UpdateString + (' WHERE  [WOID] = ' ) + (CONVERT (varchar (200),  ' '+@WOID_1+' ')) ) + CHAR(39)  )
--Test - SELECT @UpdateString_Exec as SecondString
SET @UpdateString_Exec = ( SELECT SUBSTRING (@UpdateString_Exec,2, (LEN (@UpdateString_Exec)-2)) )
--Test - SELECT @UpdateString_Exec as ThirdString
EXECUTE (@UpdateString_Exec)

SELECT @@ERROR AS ERROR_Returned

SELECT @@ROWCOUNT AS Rowcount_Returned







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

