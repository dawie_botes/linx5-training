use master
GO

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'TrainingDB') DROP DATABASE [TrainingDB]
GO

CREATE DATABASE [TrainingDB]  ON (NAME = N'TrainingDB_data', 
FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLSERVER2008\MSSQL\DATA\TrainingDB_Data.MDF' , 
SIZE = 10, 
FILEGROWTH = 10%) 
LOG ON (NAME = N'TrainingDB_log', 
FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLSERVER2008\MSSQL\DATA\TrainingDB_Log.LDF' , 
SIZE = 10, 
FILEGROWTH = 10%)
COLLATE SQL_Latin1_General_CP1_CI_AS
GO

--exec sp_dboption N'TrainingDB', N'autoclose', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'bulkcopy', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'trunc. log', N'true'
--GO

--exec sp_dboption N'TrainingDB', N'torn page detection', N'true'
--GO

--exec sp_dboption N'TrainingDB', N'read only', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'dbo use', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'single', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'autoshrink', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'ANSI null default', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'recursive triggers', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'ANSI nulls', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'concat null yields null', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'cursor close on commit', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'default to local cursor', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'quoted identifier', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'ANSI warnings', N'false'
--GO

--exec sp_dboption N'TrainingDB', N'auto create statistics', N'true'
--GO

--exec sp_dboption N'TrainingDB', N'auto update statistics', N'true'
--GO

--if( ( (@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 724) ) or ( (@@microsoftversion / power(2, 24) = 7) and (@@microsoftversion & 0xffff >= 1082) ) )
--	exec sp_dboption N'TrainingDB', N'db chaining', N'false'
--GO


USE [TrainingDB]
GO
