if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblOrder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblOrder]
GO

CREATE TABLE [dbo].[tblOrder] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[ClientCode] [varchar] (50) NULL ,
	[SecurityCode] [varchar] (50) NULL ,
	[TradeType] [varchar] (50) NULL ,
	[Price] [decimal](18, 2) NULL ,
	[Quantity] [decimal](18, 2) NULL ,
	[OrderNo] [varchar] (50) NULL ,
	[DateOfTrade] [datetime] NULL 
) ON [PRIMARY]
GO
