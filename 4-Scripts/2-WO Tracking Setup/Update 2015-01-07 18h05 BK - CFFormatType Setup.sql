--CFFORMATTYPE:
Set Identity_Insert CFFormatType ON
Insert into CFFormatType (CFFormatTypeID, Name, Description)
Values(2, 'PACS File', 'PACS File')
Insert into CFFormatType (CFFormatTypeID, Name, Description)
Values(3, 'Order File', 'Order File')
Set Identity_Insert CFFormatType OFF