--dbo.WORelationshipType insert
SET IDENTITY_INSERT dbo.WORelationshipType ON
INSERT INTO dbo.WORelationshipType (WORelationshipTypeID, Name, [Description])
VALUES(1, 'Parent', 'WO (left) is the parent of other WO''s (right)')
INSERT INTO dbo.WORelationshipType (WORelationshipTypeID, Name, [Description])
VALUES(2, 'Link', 'WO (left) is the source and WO (right) is the target')
SET IDENTITY_INSERT dbo.WORelationshipType OFF