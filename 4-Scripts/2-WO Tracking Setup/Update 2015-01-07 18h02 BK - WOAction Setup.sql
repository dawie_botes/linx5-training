--WOACTION:
Set Identity_Insert WOAction ON
Insert into WOAction (WOActionID, Name, Description) Values (1, 'Read File', 'Read file and store contents')
Insert into WOAction (WOActionID, Name, Description) Values (2, 'Parse File', 'Parse file content and extract field data')
Insert into WOAction (WOActionID, Name, Description) Values (3, 'Create WO', 'WO is created')
Insert into WOAction (WOActionID, Name, Description) Values (4, 'Close WO', 'Close WO and create child WO''s')
Insert into WOAction (WOActionID, Name, Description) Values (5, 'Parse Record', 'Parse and extraxt fields from data record')
Insert into WOAction (WOActionID, Name, Description) Values (6, 'Write data fields', 'Write/log data fields to a database table')
Insert into WOAction (WOActionID, Name, Description) Values (7, 'Duplicate Check', 'Check the current WO and confirm that another instruction does not already exist')
Insert into WOAction (WOActionID, Name, Description) Values (8, 'Validate File Format', 'Validate file format by testing for specified file content')
Insert into WOAction (WOActionID, Name, Description) Values (9, 'Validate File Duplicate', 'Validate file content by testing if it is an exact duplicate of another file')
Insert into WOAction (WOActionID, Name, Description) Values (10, 'Validate File Format follow-up', 'File Validation failed')
Insert into WOAction (WOActionID, Name, Description) Values (11, 'Validate File Duplicate follow-up', 'File is a duplicate of a previous file')
Insert into WOAction (WOActionID, Name, Description) Values (12, 'Translate Record', 'Translate Record')
Insert into WOAction (WOActionID, Name, Description) Values (13, 'Exception Followup', 'Exception Followup')
Set Identity_Insert WOAction OFF