--CFFORMAT:
Set Identity_Insert CFFormat ON
Insert into CFFormat(CFFormatID, Name, Description, CFFormatTypeID)
Values (2, 'PACS File Format', 'PACS File Format', 2)
Insert into CFFormat(CFFormatID, Name, Description, CFFormatTypeID,FileDelimiter,IgnoreFooterNoLines,IgnoreHeaderNoLines)
Values (3, 'GIS File', 'GIS File', 3,',',1,1)
Set Identity_Insert CFFormat OFF