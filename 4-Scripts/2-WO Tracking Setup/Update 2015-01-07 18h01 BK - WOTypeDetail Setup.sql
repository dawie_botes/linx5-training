--WOTYPEDETAIL:
Set Identity_Insert WOTypeDetail ON
Insert into WOTypeDetail (WODetailTypeID,Name, Description, WOLocationTable, WOTypeID)
Values (1, 'PACS Import File Detail', 'PACS Import File Detail' , 'WODFileLog' ,1)
Insert into WOTypeDetail (WODetailTypeID,Name, Description, WOLocationTable, WOTypeID)
Values (2, 'Order File', 'OrderFile' , 'WODFileLog' ,2)
Insert into WOTypeDetail (WODetailTypeID,Name, Description, WOLocationTable, WOTypeID)
Values (3, 'Order Record', 'Order Record' , 'WODSrcOrder' ,3)
Set Identity_Insert WOTypeDetail OFF