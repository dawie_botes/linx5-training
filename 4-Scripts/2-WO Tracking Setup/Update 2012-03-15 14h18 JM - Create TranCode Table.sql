
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TranType](
	[TranTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50),
	[Description] [varchar](100),
 CONSTRAINT [TranType_PK] PRIMARY KEY CLUSTERED 
(
	[TranTypeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TranCode](
	[TranCodeID] [int] IDENTITY(1,1) NOT NULL,
	[SourceSystem] [int] NOT NULL,
	[TargetSystem] [int] NOT NULL,
	[SourceValue] [varchar](100),
	[TargetValue] [varchar](100) ,
	[TranTypeID] [int] NOT NULL,
	[Active] [char](1),
	[SourceField] [varchar](50),
	[TargetField] [varchar](50),
 CONSTRAINT [TranCode_PK] PRIMARY KEY CLUSTERED 
(
	[TranTypeID] ASC,
	[TargetValue] ASC,
	[SourceValue] ASC,
	[TargetSystem] ASC,
	[SourceSystem] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_TranCode] UNIQUE NONCLUSTERED 
(
	[TranCodeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TranCode]  WITH NOCHECK ADD  CONSTRAINT [CFSystem_TranCode_FK1] FOREIGN KEY([SourceSystem])
REFERENCES [dbo].[CFSystem] ([CFSystemID])
GO
ALTER TABLE [dbo].[TranCode] CHECK CONSTRAINT [CFSystem_TranCode_FK1]
GO
ALTER TABLE [dbo].[TranCode]  WITH NOCHECK ADD  CONSTRAINT [CFSystem_TranCode_FK2] FOREIGN KEY([TargetSystem])
REFERENCES [dbo].[CFSystem] ([CFSystemID])
GO
ALTER TABLE [dbo].[TranCode] CHECK CONSTRAINT [CFSystem_TranCode_FK2]
GO
ALTER TABLE [dbo].[TranCode]  WITH CHECK ADD  CONSTRAINT [TranType_TranCode_FK1] FOREIGN KEY([TranTypeID])
REFERENCES [dbo].[TranType] ([TranTypeID])