/*
WOTypeID 1 is the PACS Import File
WOTypeID 2 is the Order File
WOTypeID 3 is the Order Record
*/
--WOTYPE:
Set Identity_Insert WOType ON
Insert into WOType (WOTypeID, Name, Description, WOPriorityIDDefault, WOStatusIDDefault)
Values(1,'PACS Import File', 'PACS Import File', 1, 1)
Insert into WOType (WOTypeID, Name, Description, WOPriorityIDDefault, WOStatusIDDefault)
Values(2,'Order File', 'Order File', 1, 1)
Insert into WOType (WOTypeID, Name, Description, WOPriorityIDDefault, WOStatusIDDefault)
Values(3,'Order Record', 'Order Record', 1, 1)
Set Identity_Insert WOType OFF