USE TrainingDB
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'prc_WODSrcOrder_Insert')
BEGIN
DROP PROCEDURE dbo.prc_WODSrcOrder_Insert
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================
-- Author:		<Byron Kilian (Digiata) aka Thor>
-- Create date: <2015/01/07>
-- Description:	<This store procedure inserts into table dbo.WODSrcOrder>
-- ==========================================================================
CREATE PROCEDURE [dbo].[prc_WODSrcOrder_Insert]
-- Add the parameters for the stored procedure here
@WOID INT,
@TransactionDate DATETIME,
@AssetCodeForm VARCHAR(10),
@FundName VARCHAR(100),
@ProductName VARCHAR(100),
@InvestDisInvestAmount DECIMAL(16,3),
@DisInvestUnits INT,
@MancoUTFee DECIMAL(16,3),
@TransactionTypeName VARCHAR(5),
@AdminCommission DECIMAL(16,3),
@FundingTranReversalAmount DECIMAL(16,3),
@ReversalIndicator VARCHAR(5),
@PaymentMethod VARCHAR(5),
@InternalRefNo INT,
@SSVSNo VARCHAR(20),
@TransactionType VARCHAR(20),
@Success INT OUTPUT,
@Message VARCHAR(50) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
BEGIN TRY
	BEGIN TRAN;
	INSERT INTO [TrainingDB].[dbo].[WODSrcOrder]
           ([WOID]
           ,[TransactionDate]
           ,[AssetCodeForm]
           ,[FundName]
           ,[ProductName]
           ,[InvestDisInvestAmount]
           ,[DisInvestUnits]
           ,[MancoUTFee]
           ,[TransactionTypeName]
           ,[AdminCommission]
           ,[FundingTranReversalAmount]
           ,[ReversalIndicator]
           ,[PaymentMethod]
           ,[InternalRefNo]
           ,[SSVSNo]
           ,[TransactionType])
     VALUES
           (@WOID
           ,@TransactionDate
           ,@AssetCodeForm
           ,@FundName
           ,@ProductName
           ,@InvestDisInvestAmount
           ,@DisInvestUnits
           ,@MancoUTFee
           ,@TransactionTypeName
           ,@AdminCommission
           ,@FundingTranReversalAmount
           ,@ReversalIndicator
           ,@PaymentMethod
           ,@InternalRefNo
           ,@SSVSNo
           ,@TransactionType)
	COMMIT TRAN;
SET @Success = 1
SET @Message = 'Insert successful'	
END TRY	
BEGIN CATCH
IF @@TRANCOUNT > 0 ROLLBACK TRAN;
SET @Success = 0
SET @Message = 'Insert Failed: ' + ERROR_MESSAGE() 
END CATCH

SELECT 
Successful = @Success, 
[Message] = @Message
	
END

GO