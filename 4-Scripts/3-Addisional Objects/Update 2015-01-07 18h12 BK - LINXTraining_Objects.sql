--This dinamic script creates the objects required for LINX training:
USE TrainingDB
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'WODSrcOrder')
BEGIN
DROP TABLE dbo.WODSrcOrder
END
GO
CREATE TABLE dbo.WODSrcOrder
(WODSrcOrderID INT IDENTITY(1,1) NOT NULL,
 WOID INT NOT NULL,
 TransactionDate DATETIME,
 AssetCodeForm VARCHAR(10),
 FundName VARCHAR(100),
 ProductName VARCHAR(100),
 InvestDisInvestAmount DECIMAL(16,3),
 DisInvestUnits INT,
 MancoUTFee DECIMAL(16,3),
 TransactionTypeName VARCHAR(5),
 AdminCommission DECIMAL(16,3),
 FundingTranReversalAmount DECIMAL(16,3),
 ReversalIndicator VARCHAR(5),
 PaymentMethod VARCHAR(5),
 InternalRefNo INT,
 SSVSNo VARCHAR(20),
 TransactionType VARCHAR(20),
 PRIMARY KEY (WOID)
 )
GO 
--========================================================================================= 
IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'TranType')
BEGIN
Set Identity_Insert TranType ON
Insert into TranType(TranTypeID,Name,[Description])
Values(1,'GIS Transaction Type','GIS Transaction Type')
Set Identity_Insert TranType OFF
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'TranCode')
BEGIN
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'61M','Purchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'42','Repurchase',1,1)	
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'65L','Purchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'64A','Repurchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'61A','Purchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'LAO','Purchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'LAI','Repurchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'65W','Purchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'65C','Purchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'OFA','Purchase',1,1)
Insert into TranCode(SourceSystem,TargetSystem,SourceValue,TargetValue,TranTypeID,Active)
Values(3,3,'64B','Repurchase',1,1)
END
GO
--========================================================================================= 
IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'WODSrcPACS')
BEGIN
DROP TABLE dbo.WODSrcPACS
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WODSrcPACS](
	[WODSrcPACSId] [int] IDENTITY(1,1) NOT NULL,
	[WOID] [int] NULL,
	[RecordIdentifier] [int] NULL,
	[UserBranch] [int] NULL,
	[UserAccountNumber] [varchar](11) NULL,
	[UserCode] [varchar](4) NULL,
	[UserSequenceNumber] [int] NULL,
	[HomingBranch] [int] NULL,
	[HomingAccountNumber] [varchar](11) NULL,
	[TypeOfAccount] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[ActionDate] [datetime] NULL,
	[EntryClass] [int] NULL,
	[TaxCode] [int] NULL,
	[UserReference] [varchar](30) NULL,
	[HomingAccountName] [varchar](30) NULL,
	[NonStandardAccountNumber] [varchar](20) NULL,
	[HomingInstitution] [varchar](2) NULL,
 CONSTRAINT [WODSrcPACS_PK] PRIMARY KEY CLUSTERED 
(
	[WODSrcPACSId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF