IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'prc_WordDocumentListBK_Insert')
BEGIN
DROP PROCEDURE dbo.prc_WordDocumentListBK_Insert 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Byron Kilian (aka Thor)>
-- Create date: <2015/01/13>
-- Description:	<Insert into WordDocumentListBK>
-- =============================================
CREATE PROCEDURE dbo.prc_WordDocumentListBK_Insert 
(
@FileName VARCHAR(MAX),
@DateCreated DATETIME,
@DateModified DATETIME,
@FileSize INT
)
AS
BEGIN
INSERT INTO dbo.WordDocumentListBK ([FileName], DateCreated, DateModified, FileSize)
VALUES(@FileName, @DateCreated, @DateModified, @FileSize)
END
GO
