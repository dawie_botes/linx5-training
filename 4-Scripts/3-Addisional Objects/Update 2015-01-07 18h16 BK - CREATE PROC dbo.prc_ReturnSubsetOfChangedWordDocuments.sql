IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'prc_ReturnSubsetOfChangedWordDocuments')
BEGIN
DROP PROCEDURE dbo.prc_ReturnSubsetOfChangedWordDocuments
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Byron Kilian (aka Thor)>
-- Create date: <2015/01/13>
-- Description:	<Return subset of Files>
-- =============================================
CREATE PROCEDURE dbo.prc_ReturnSubsetOfChangedWordDocuments 
	-- Add the parameters for the stored procedure here
(
@Date DATETIME
)
AS
BEGIN
SET @Date = CONVERT(DATE, @Date)
SELECT
[FileName],
DateModified
FROM dbo.WordDocumentListBK (NOLOCK)
WHERE CONVERT(DATE, DateModified) >= @Date
END
GO
