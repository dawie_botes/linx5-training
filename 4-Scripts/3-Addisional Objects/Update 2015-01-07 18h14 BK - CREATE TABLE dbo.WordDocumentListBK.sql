IF EXISTS (SELECT 1 FROM sys.objects WHERE [name] = 'WordDocumentListBK')
BEGIN
DROP TABLE dbo.WordDocumentListBK 
END
GO
--This table is created with your initials:
CREATE TABLE dbo.WordDocumentListBK 
	(ID INT PRIMARY KEY IDENTITY(1,1), 
	[FileName] VARCHAR(MAX) NULL, 
	DateCreated DATETIME NULL, 
	DateModified DATETIME NULL, 
	FileSize INT NULL)