# README #

This repository contains the Training for Linx 5.

Download Linx here: [http://linx.twenty57.com](http://linx.twenty57.com)

### Structure ###

1. Documents
2. Linx Installation
3. TrainingDB Setup
4. Scripts
5. Solutions
6. SourceFiles
7. TargetFiles
8. Feedback